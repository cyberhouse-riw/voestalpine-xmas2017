var noderesolve = require('rollup-plugin-node-resolve');
var commonjs = require('rollup-plugin-commonjs');
var babel = require('rollup-plugin-babel');
var uglify = require('rollup-plugin-uglify');

var replace = require('postcss-replace');
var autroprefixer = require('autoprefixer');

var gutil = require('gulp-util');
var fs = require('fs');
var path = require('path');


// OPTIONS

var browserOptions = {
  browsers: [
    "last 3 versions",
    "Explorer >= 11",
    "iOS >= 10"
  ]
};
var printFixWidth = 992;

exports.rollupPlugins = function(config, mode) {
  var plugins = [
    noderesolve(),
    commonjs({
      sourceMap: false
    }),
    babel({
      'presets': [[
        'env',
        {
          'targets': browserOptions,
          'modules': false,
          'loose': true
        }
      ]],
      'plugins': ['external-helpers'],
      'exclude': 'node_modules/**'
    })
  ];
  if(mode === "prod") {
    plugins.push(uglify());
  }
  return plugins;
};

exports.postcssPlugins = function(config, mode) {
  return [
    function (root, result) {
      root.walkAtRules('media', function (rule) {
        rule.params = rule.params.replace(/^((?:(?!screen|print|speech)[^{,])*?)\(\s*(min|max)-width\s*:\s*(\d+)px\s*\)(?:((?:(?!screen|print|speech)[^{,])+?)\(\s*(min|max)-width\s*:\s*(\d+)px\s*\))?$/, function(str, pre1, cond1, v1, pre2, cond2, v2) {
          v1 = parseInt(v1);
          if(cond2 === undefined) {
            if (cond1 === 'min' && printFixWidth >= v1 || cond1 === 'max' && printFixWidth <= v1) {
              pre1 = 'print, screen and ' + pre1.replace(/\ball( and|,)/, '');
            }
            else if(cond1 === 'max' && printFixWidth > v1) {
              pre1 = 'screen and ' + pre1.replace(/\ball( and|,)/, '');
            }
            return pre1+'('+cond1+'-width: '+v1+'px)';
          }
          else {
            v2 = parseInt(v2);
            if (cond1 === 'min' && cond2 === 'max' && printFixWidth >= v1 && printFixWidth <= v2 || cond1 === 'max' && cond2 === 'min' && printFixWidth >= v2 && printFixWidth <= v1) {
              pre1 = 'print, screen and ' + pre1.replace(/\ball( and|,)/, '');
              pre2 = pre2.replace(/\ball( and|,)/, '');
            }
            else if(cond1 === 'max' && printFixWidth > v1 || cond2 === 'max' && printFixWidth > v2) {
              pre1 = 'screen and ' + pre1.replace(/\ball( and|,)/, '');
            }
            return pre1+'('+cond1+'-width: '+v1+'px)'+pre2+'('+cond2+'-width: '+v2+'px)';
          }
        });
      });
    },
    autroprefixer(browserOptions)
  ];
};

exports.nunjucksFilters = function(config, mode) {
  var base = config.templates.options.nunjucks.path || './';

  return {
    icon: function (prefix, file, attributes, alt) {
      var key = prefix + 'icons';
      var icons;
      try {
        icons = this.env.getGlobal(key);
      }
      catch (err) {
        icons = {};
        this.env.addGlobal(key, icons);
      }
      key = path.basename(file, path.extname(file));
      icons[key] = file;
      return '<svg role="img"' + (attributes !== undefined ? ' ' + attributes : '') + (alt !== undefined ? ' aria-label="' + alt + '"><title>' + alt + '</title>' : ' aria-hidden="true">') + '<use xlink:href="#' + prefix + key + '"></use></svg>';
    },
    icons: function (prefix) {
      try {
        var key = prefix + 'icons';
        var icons = this.env.getGlobal(key);
        this.env.addGlobal(key, {});
        var result = Object.keys(icons).map(function (key) {
          try {
            var icon = path.join(base, icons[key]), viewbox;
            icon = fs.readFileSync(icon, 'UTF8');
            icon = icon.replace(/\s*(<\?[\s\S]*?\?>|<!--[\s\S]*?-->|<\/svg>)\s*/g, '');
            icon = icon.replace(/<svg[^>]*>/i, function (svgtag) {
              var vbmatch = svgtag.match(/\bviewBox\s*=\s*".*?"/i);
              viewbox = (vbmatch ? ' ' + vbmatch[0] : '');
              return '';
            });
            return '<symbol id="' + prefix + key + '"' + viewbox + '>' + icon + '\n</symbol>';
          }
          catch (err) {
            gutil.log(gutil.colors.red(err.message));
            return '<!-- ' + err.message + ' -->';
          }
        });
        return '<svg xmlns="http://www.w3.org/2000/svg" style="display: none;" aria-hidden="true" data-icons>\n' + result.join('\n') + '\n</svg>';
      }
      catch (err) {
        return '';
      }
    },
    content: function (file) {
      try {
        return fs.readFileSync(path.join(base, file), 'UTF8');
      }
      catch (err) {
        gutil.log(gutil.colors.red(err.message));
        return '<!-- ' + err.message + ' -->';
      }
    }
  };
};