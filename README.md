# Cyberhouse Frontend Workflow

a starter kit for cyberhouse frontend projects containing the gulp workflow and some sample files to start from.

## this repository contains:
* *.csscomb.json, .jscsrc* (code style files)
* *.gitignore* (ignore list)
* *gulpfile.js* (the main file that defines the tasks)
* *config.json* (a base config file)
* *configx.js* (an extended configuration containing functions used with `x:` in *config.json*)
* *package.json, package-lock.json* (the node packages needed for the workflow)
* *.nvmrc* (a file containing the node version number needed for this project)
* *README.md* (this file)
* *src* (a folder with some dummy source files)
* *build/assets* (a folder containig some dummy assets)

## dependencies:

* nvm

## how to use it:

### quickstart

```
nvm install && npm install --only=prod
```

* `nvm install` installs the right node and npm version
* `npm install` installs the node modules and runs the gulp default task in prod mode (to install dev dependencies, too, use the flag `--only=dev` instead)

### gulp commands

* simple tasks:
    * **`clean`**: remove all created files
    * **`favicons`**: generate the favicons and the favicon.html
    * **`img`**: build images
    * **`js`**: bundle js files
    * **`css`**: build styles
    * **`templates`**: build templates
    * **`copy`**: run copy tasks
* watch tasks:
    * **`watch-img`**: watch source images and build when updated
    * **`watch-js`**: watch source javascript files and build when updated
    * **`watch-css`**: watch source styles and build when updated
    * **`watch-templates`**: watch source templates and build when updated
    * **`watch-copy`**: watch files to copy and copy when updated
* combined tasks:
    * *`default`*: run `favicons`, `img`, `js` and `css` parallel followed by `templates` and `copy` in sequence
    * **`watch`**: run `default` followed by `watch-img`, `watch-bundles`, `watch-js`, `watch-scss`, `watch-templates` and `watch-copy`
* extraordinary tasks (only available with dev dependencies):
    * **`serve`**: run `watch` and serve the generated files by a browser-sync server instance
    * **`deploy`**: run `default` and rsync the generated files to a given destination
    
all commands can have a `--mode` parameter to define the mode in which to run

the task `default` has a switch `--clean` to run the clean task before the rest (otherwise in only generates the needed destination files if they don't exist or the source files are newer)

### configurations

#### modes.json

this is the default modes configuration that is used if no *modes.json* file exists

```
{
    "list": ["dev", "prod"],
    "default": "dev"
}
```

* **`list`** is an array of all possible modes (e.g.: for multi theme projects you can have each of these modes for every theme)
* **`default`** is the mode that should be used if no valid `--mode` parameter is given

#### config.json

basic structure in pseudo code

```
{
    "task": {
        "src": ...,
        "watch": ...,
        "options": {
            ...
        },
        "dest": ...
        "clean": ...
    }
}
```

* **`task`**: exists for every simple gulp task (except of `clean`) and can be omitted or set to `null` to be skipped. alternatively omitting the option `dest` or setting it to `null` has the same result. if defined `task` can be an object configuring one task or an array of objects for multiple configurations of parallel running tasks.
    * **`src`**: string or array of strings (globs) defining the source files used.
    * **`watch`**: optional string or array of strings of files (globs) used for `gulp watch` (if not defined `src` is used for watching).
    * **`options`**: an object containing the settings for all the gulp plugins used in `task`.
    * **`dest`**: string or array of strings defining the destination folder(s) where the resulting files should be put.
    * **`clean`**: optional list of files/folders (globs) that should be used to clean this task up (if not defined `dest` is used for clean up).
    
every value of these objects can be replaced by an object containing only modes as keys. they will be resolved for the mode used to run the task. e.g.:

```
{
    "task": {
        "src": ...
        ...
        "dest": {
            "dev": "build/dev",
            "prod": "build/prod"
        }
    }
}
```

this task will have a `dest` only in `dev` or `prod` mode and won't run in any other because `dest` is not defined (and as described above a task won't run without `dest` given). a similar result could be achieved with the following code:

```
{
    "task": {
        "dev": {
            "src": ...
            ...
            "dest": "build/dev"
        }
        "prod": {
            "src": ...
            ...
            "dest": "build/prod"
        }
    }
}
```

the tasks defined in the *config.json* can have the following options:

* **`favicons`**
    * `favicons`: the options for the favicons plugin [more](https://github.com/haydenbleasel/favicons).
* **`img`**
    * `imagemin`: the options for the imagemin plugin [more](https://github.com/sindresorhus/gulp-imagemin).
* **`js`**
    * `rollup`: the options for the rollup-stream plugin [more](https://github.com/Permutatrix/rollup-stream) where `plugins` can be a string defining a javascript module which exports the plugins array (see `plugins.js`).
* **`css`**
    * `scss`: the options for the sass plugin [more](https://github.com/dlmanning/gulp-sass).
    * `postcss`: the options for the postcss plugin [more](https://github.com/postcss/gulp-postcss).
* **`templates`**
    * `nunjucks`: the options for the nunjucks plugin [more](https://github.com/carlosl/gulp-nunjucks-render). there are three custom filters defined for this plugin as described below. You can add more filters via the `filters` option set as path to a file containing your filter implementations (see `filters.js`).
* **`copy`**
    * in this task there are no plugins used.
* `serve`

    the extraordinary task `serve` defines the settings object for the browser-sync plugin [more](https://github.com/Browsersync/browser-sync). the mode splitting can be used here, too, as described for the other tasks.
* `deploy`

    the extraordinary task `deploy` defines the settings object for the gulp-rsync plugin [more](https://github.com/jerrysu/gulp-rsync). the mode splitting can be used here, too, as described for the other tasks.

##### custom filters for nunjucks templates:

* `{{ 'prefix-' | icon('path/to/filename.svg', 'class="icon" data-attribute="value"', 'title') }}`: globally collects the given svg file and writes an svg tag using the symbol with the id `prefix-filename` of a hidden svg (written by the next filter) and optionally adds attributes like `class="icon" data-attribute="value"` and `aria-label="title"`/`<title>title</title>` resp. `aria-hidden="true"` to the tag.
* `{{ 'prefix-' | icons }}`: writes a hidden inline svg defining all the symbols collected by the previous filter with the given prefix and clears the file collection. This filter should be used at the end of the document so that really all used symbols are written.
* `{{ 'path/to/filename.xyz' | content }}`: replaces this expression with the content of the given file.

please note that the autoescape option for the nunjucks plugin has to be set to false for those filters to work correctly.

##### for the deployment:

Run npm install in production mode `npm install --only=prod` to prevent dev dependencies to be installed.

Images and favicons will only be processed in `dev` mode.

sometimes you have to clean the frontend workspace to remove unused files etc. if you don't want to wipe out the whole deployment workspace you can just add the switch `--clean` to your gulp task (for one run).