(function (undefined) {

  if (!Detector.webgl) {
    Detector.addGetWebGLMessage();
  } else {
    function updateUVs(object, camera) {
      object.updateMatrixWorld();
      camera.updateMatrixWorld();
      var vertices = object.geometry.vertices;
      var verticeslength = vertices.length;
      var uvs = new Array(verticeslength);
      for (var vertexindex = 0, vertex; vertexindex < verticeslength; vertexindex++) {
        vertex = vertices[vertexindex].clone();
        vertex = object.localToWorld(vertex);
        vertex = vertex.project(camera);
        vertex = new THREE.Vector2((vertex.x + 1) / 2, (vertex.y + 1) / 2);
        uvs[vertexindex] = vertex;
      }

      var faces = object.geometry.faces;
      var faceslength = faces.length;
      var uvlayer0 = object.geometry.faceVertexUvs[0];
      for (var faceindex = 0, face, faceuvs; faceindex < faceslength; faceindex++) {
        face = faces[faceindex];
        faceuvs = uvlayer0[faceindex];
        faceuvs[0] = uvs[face.a];
        faceuvs[1] = uvs[face.b];
        faceuvs[2] = uvs[face.c];
      }

      object.geometry.uvsNeedUpdate = true;
    }

    function bakePlaneTexture(scene, camera, plane, widthSegments, heightSegments, textureWidth) {
      var width = plane.geometry.parameters.width;
      var height = plane.geometry.parameters.height;
      var halfwidth = width / 2;
      var halfheight = height / 2;

      var dummyscene = new THREE.Scene();

      var dummy = plane.clone();
      dummy.geometry = new THREE.PlaneGeometry(width, height, widthSegments, heightSegments);
      dummy.material = new THREE.MeshBasicMaterial({ map: plane.material.map });

      updateUVs(dummy, camera);

      dummy.position.set(0, 0, 0);
      dummy.rotation.set(0, 0, 0);
      dummy.scale.set(1, 1, 1);

      dummyscene.add(dummy);

      var dummycam = new THREE.OrthographicCamera(-halfwidth, halfwidth, -halfheight, halfheight, 0, 1);
      dummycam.position.set(0, 0, -1);
      dummycam.up.set(0, -1, 0);
      dummycam.lookAt(dummy.position);

      dummyscene.add(dummycam);

      dummycam.updateProjectionMatrix();

      var dummytex = new THREE.WebGLRenderTarget(textureWidth, textureWidth / width * height, {
        minFilter: THREE.LinearFilter,
        magFilter: THREE.NearestFilter,
        format: THREE.RGBFormat,
      });

      renderer.render(dummyscene, dummycam, dummytex, true);

      delete dummyscene;
      delete dummycam;
      delete dummytex;

      plane.material.map = dummytex.texture;
    }

    var loaded = {
      audio: false,
      bgtexture: false,
      cardfront: false,
      cardtop: false,
      cardbottom: false,
      giftsimages: false,
      gifts: false,
    };

    var audio = new Howl({
      src: ['assets/audio/audio.mp3'],
    }).once('load', function () {
      loaded.audio = true;
      checkLoading();
    });

    var texManager = new THREE.LoadingManager();
    texManager.onLoad = function () {
      loaded.bgtexture = true;
      checkLoading();
    };

    var bgtexture = new THREE.TextureLoader(texManager).load('assets/img/854089692.jpg');

    var cardfront = document.getElementById('card-front');
    loadAnimate('F6F2BA9C72734BD3A7DA14FD25435EE8', 'front', cardfront, function (target, root) {
      loaded.cardfront = true;
      checkLoading();
    }, null, cardFrontComplete);

    var cardtop = document.getElementById('card-top');
    loadAnimate('D9C0AEA2AE874F728A052EDE2D2A5A94', 'top', cardtop, function (target, root) {
      loaded.cardtop = true;
      checkLoading();
    }, null, cardTopComplete);

    var cardbottom = document.getElementById('card-bottom');
    loadAnimate('933BC9B3B3754B3D977DD753940E6191', 'bottom', cardbottom, function (target, root) {
      loaded.cardbottom = true;
      checkLoading();
    }, null, cardBottomComplete);

    var mtlManager = new THREE.LoadingManager(function () {
      loaded.giftsimages = true;
      checkLoading();
    });

    var name = 'vag_karte_v5_aufbereitet_v5';

    var gifts;
    var mtlLoader = new THREE.MTLLoader(mtlManager);
    mtlLoader.setPath('assets/model/');
    mtlLoader.load(name + '.mtl', function (materials) {
      materials.preload();
      var objLoader = new THREE.OBJLoader();
      objLoader.setMaterials(materials);
      objLoader.setPath('assets/model/');
      objLoader.load(name + '.obj', function (object) {
        gifts = object;
        loaded.gifts = true;
        checkLoading();
      });
    });

    function checkLoading() {
      var done = true;
      var keys = Object.keys(loaded);
      var index = keys.length;
      while (index-- && done) {
        done = loaded[keys[index]];
      }

      if (done) {
        doneLoading();
      }
    }

    var scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera(75, 16 / 9, 0.1, 1000);
    var lookat = new THREE.Vector3(0, 0, 0);
    var renderer = new THREE.WebGLRenderer({ antialias: true });
    onWindowResize();
    renderer.domElement.id = 'view';
    document.body.appendChild(renderer.domElement);
    window.addEventListener('resize', onWindowResize, false);

    var bg;
    var floor;
    var fixgroup;
    var cfront;
    var ctop;
    var cbottom;
    var cover;
    var back;
    var card;
    var ambient;
    var light1;
    var light2;
    var light3;

    function onWindowResize() {
      var width = window.innerWidth;
      var height = window.innerHeight;
      var aspect = width / height;
      if (aspect > camera.aspect) {
        width = height * camera.aspect;
      } else {
        height = width / camera.aspect;
      }

      renderer.setSize(width, height);
    }

    function createCard() {
      cfront = new THREE.Mesh(
        new THREE.PlaneGeometry(1.024, 0.5),
        new THREE.MeshBasicMaterial({ map: new THREE.CanvasTexture(cardfront) })
      );
      cfront.position.set(0, 0.25, -0.002);
      cfront.rotation.x = -Math.PI;

      ctop = new THREE.Mesh(
        new THREE.PlaneGeometry(1.024, 0.5),
        new THREE.MeshBasicMaterial({ map: new THREE.CanvasTexture(cardtop) })
      );
      ctop.position.set(0, 0.25, -0.002);

      cbottom = new THREE.Mesh(
        new THREE.PlaneGeometry(1.024, 0.5),
        new THREE.MeshBasicMaterial({ map: new THREE.CanvasTexture(cardbottom) })
      );
      cbottom.position.set(0, -0.25, -0.002);

      cover = new THREE.Group();
      cover.add(cfront);
      cover.add(ctop);
      cover.rotation.x = Math.PI;

      back = new THREE.Group();
      back.add(cbottom);

      card = new THREE.Group();
      card.add(cover);
      card.add(back);
    }

    function doneLoading() {
      var bgmaterial = new THREE.MeshBasicMaterial({ map: bgtexture });

      bg = new THREE.Mesh(
        new THREE.PlaneGeometry(60, 40),
        bgmaterial
      );
      bg.position.set(0, 0, -10);
      scene.add(bg);

      floor = new THREE.Mesh(
        new THREE.PlaneGeometry(60, 10, 40, 40),
        bgmaterial.clone()
      );
      floor.position.set(0, -9, -5);
      floor.rotation.x = -Math.PI * 0.5;
      scene.add(floor);

      fixgroup = new THREE.Group();
      fixgroup.position.set(-2.9, -8.7, -3.7);
      fixgroup.rotation.x = -Math.PI * 0.1;
      scene.add(fixgroup);

      gifts.position.set(0.8, 0, -1.27);
      gifts.scale.set(0.02, 0.018, 0.02);
      gifts.rotation.y = -Math.PI * 0.1;
      fixgroup.add(gifts);

      createCard();
      card.position.set(0, 1.44, 0);
      card.rotation.x = -Math.PI * 0.5;
      fixgroup.add(card);

      camera.position.set(0, 0, 10);
      camera.lookAt(lookat);
      camera.zoom = 1;
      camera.updateProjectionMatrix();

      updateUVs(bg, camera);
      bakePlaneTexture(scene, camera, floor, 40, 40, 2048);

      ambient = new THREE.AmbientLight(0xffffff, 0.6);
      scene.add(ambient);

      light1 = new THREE.PointLight(0xffffff, 0.6);
      light1.position.set(-5, 0, 5);
      scene.add(light1);

      light2 = new THREE.PointLight(0xffffff, 0.2);
      light2.position.set(5, 0, 5);
      scene.add(light2);

      light3 = new THREE.PointLight(0xffffff, 0.2);
      light3.position.set(-5, -5, -5);
      scene.add(light3);

      document.body.classList.remove('loading');
      startAnimation();
    }

    var timeline;
    var start;

    function startAnimation() {
      audio.play();

      timeline = anime.timeline({
        autoplay: false,
        complete: function () {
          cardfront.playAnimate();
        },
      });
      timeline.add({
        targets: fixgroup.rotation,
        x: 0,
        offset: 2000,
        duration: 4000,
        easing: 'easeInOutQuad',
      }).add({
        targets: fixgroup.position,
        y: -9,
        z: -4.5,
        offset: 2000,
        duration: 5000,
        easing: 'easeInOutQuad',
      }).add({
        targets: camera.position,
        x: -2.9,
        offset: 2000,
        duration: 2000,
        easing: 'easeInOutQuad',
      }).add({
        targets: camera.position,
        y: -0.8,
        z: -4.25,
        offset: 4200,
        duration: 4000,
        easing: 'easeInOutQuad',
      }).add({
        targets: lookat,
        x: -2.9,
        y: -7.56,
        z: -4.25,
        offset: 2300,
        duration: 2000,
        easing: 'easeInOutQuad',
        update: function () {
          camera.lookAt(lookat);
        },
      }).add({
        targets: camera,
        zoom: 18,
        offset: 2000,
        duration: 5000,
        easing: 'easeInOutQuad',
      });

      start = Date.now();
      animate();
    }

    var completed = {
      front: false,
      top: false,
      bottom: false,
    };

    function cardFrontComplete(target, root) {
      completed.front = true;
      timeline = anime.timeline({
        autoplay: false,
        complete: function () {
          cardtop.playAnimate();
        },
      });
      timeline.add({
        targets: cover.rotation,
        x: 0,
        offset: 0,
        duration: 2000,
        easing: 'easeInOutQuad',
      }).add({
        targets: camera.position,
        z: -4.75,
        offset: 0,
        duration: 2000,
        easing: 'easeInOutQuad',
      });
      start = Date.now();
    }

    function cardTopComplete(target, root) {
      completed.top = true;
      timeline = anime.timeline({
        autoplay: false,
        complete: function () {
          cardbottom.playAnimate();
        },
      });
      timeline.add({
        targets: camera.position,
        z: -4.25,
        offset: 0,
        duration: 2000,
        easing: 'easeInOutQuad',
      });
      start = Date.now();
    }

    function cardBottomComplete(target, root) {
      completed.bottom = true;
    }

    function animate() {
      requestAnimationFrame(animate);

      if (!timeline.completed) {
        var t = Date.now() - start;
        timeline.seek(t);
        camera.updateProjectionMatrix();
      }

      if (!completed.front) {
        cfront.material.map.needsUpdate = true;
      } else {
        ctop.material.map.needsUpdate = true;
        if (completed.top && !completed.bottom) {
          cbottom.material.map.needsUpdate = true;
        }
      }

      renderer.render(scene, camera);
    }
  }

})();