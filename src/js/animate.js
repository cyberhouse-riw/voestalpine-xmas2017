function loadAnimate(composition, root, target, onLoaded, onUpdate, onComplete) {
  var comp = AdobeAn.getComposition(composition);
  var loader = new createjs.LoadQueue(false);
  var lib = comp.getLibrary();
  if (lib.properties.manifest.length > 0) {
    loader.addEventListener('fileload',
      function (evt) {
        var img = comp.getImages();
        if (evt && (evt.item.type === 'image')) {
          img[evt.item.id] = evt.result;
        }
      }
    );
    loader.addEventListener('complete',
      function (evt) {
        loader.removeEventListener('fileload');
        loader.removeEventListener('complete');
        loadAnimateComplete(evt, comp, root, target, onLoaded, onUpdate, onComplete);
      }
    );
    loader.loadManifest(lib.properties.manifest);
  } else {
    loadAnimateComplete({}, comp, root, target, onLoaded, onUpdate, onComplete);
  }
}

function loadAnimateComplete(evt, comp, root, target, onLoaded, onUpdate, onComplete) {
  var lib = comp.getLibrary();
  var ss = comp.getSpriteSheet();
  var queue = evt.target;
  var ssMetadata = lib.ssMetadata;
  for (i = 0; i < ssMetadata.length; i++) {
    ss[ssMetadata[i].name] = new createjs.SpriteSheet({
      images: [queue.getResult(ssMetadata[i].name)],
      frames: ssMetadata[i].frames,
    });
  }

  var stage = new lib.Stage(target);
  var exportRoot = new lib[root]();

  target.playAnimate = function () {
    exportRoot.play();
  };

  target.stopAnimate = function () {
    exportRoot.stop();
  };

  target.stopAnimate();

  if (onLoaded || onUpdate || onComplete) {
    exportRoot.timeline.addEventListener('change', function (event) {
      if (onUpdate) {
        onUpdate(target, exportRoot);
      }

      if (event.target.position === 0) {
        if (onLoaded) {
          onLoaded(target, exportRoot);
          onLoaded = null;
        }
      } else if (event.target.position === event.target.duration) {
        if (onComplete) {
          onComplete(target, exportRoot);
        }
      }
    });
  }

  target.width = lib.properties.width;
  target.height = lib.properties.height;
  stage.addChild(exportRoot);

  AdobeAn.compositionLoaded(lib.properties.id);
  createjs.Ticker.setFPS(lib.properties.fps);
  createjs.Ticker.addEventListener('tick', stage);
}