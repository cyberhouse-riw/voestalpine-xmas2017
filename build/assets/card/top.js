(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.top_1 = function() {
	this.initialize(img.top_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,500);


(lib.light = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Ebene 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0.8)","rgba(237,210,132,0)"],[0,1],0,0,0,0,0,31).s().p("AjZDaQhZhbgBh/QABh+BZhbQBbhZB+gBQB/ABBbBZQBZBbABB+QgBB/hZBbQhbBZh/ABQh+gBhbhZg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30.7,-30.7,61.5,61.5);


(lib.headline = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{de:1,en:2});

	// timeline functions:
	this.frame_0 = function() {
		this.gotoAndStop(window.card.lang);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// headline
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKALQgEgEAAgGQAAgHAEgEQAFgFAFABQAHgBAEAFQAEAEAAAHQAAAGgEAEQgEAFgHgBQgFABgFgFg");
	this.shape.setTransform(479.5,80.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_1.setTransform(465.9,72.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_2.setTransform(448.8,72.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_3.setTransform(430.4,72.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_4.setTransform(409.4,72.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhCBaQgbgbAAgoQAAgnAbgbQAbgbAnAAQAoAAAbAbQAbAbAAAnQAAAogbAbQgbAbgoAAQgnAAgbgbgAg2ggQgXAWAAAhQAAAiAXAWQAWAXAgAAQAhAAAXgXQAWgWAAgiQAAghgWgWQgXgXghAAQggAAgWAXgAAThdQgEgEAAgGQAAgGAEgEQAEgDAGAAQAFAAAEADQAEAEAAAGQAAAGgEAEQgEAEgFAAQgGAAgEgEgAglhdQgEgEAAgGQAAgGAEgEQAEgDAGAAQAFAAAEADQAEAEAAAGQAAAGgEAEQgEAEgFAAQgGAAgEgEg");
	this.shape_5.setTransform(388.1,70.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAtBcIhghYIAABYIgQAAIAAi3IAQAAIAABbIBbhbIAVAAIhcBdIBjBag");
	this.shape_6.setTransform(370.4,72.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_7.setTransform(343.7,72.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_8.setTransform(326.6,72.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AA5BcIAAhXIhxAAIAABXIgQAAIAAi3IAQAAIAABRIBxAAIAAhRIAQAAIAAC3g");
	this.shape_9.setTransform(308.6,72.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgwBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAiAAAZAUIgJANQgWgSgcAAQggAAgXAXQgWAWAAAhQAAAiAWAWQAXAXAgAAQAdAAAVgSIAJANQgZAUgiAAQgnAAgbgbg");
	this.shape_10.setTransform(290,72.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_11.setTransform(271.9,72.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("ABJBdIAAiQIhJBSIhIhSIAACQIgQAAIAAi5IBYBkIBZhkIAAC5g");
	this.shape_12.setTransform(251.3,72.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAsBcIhEhTIgYAAIAABTIgPAAIAAi3IAwAAQAcAAAQANQAPANABAYQgBAWgNANQgNAMgXADIBFBTgAgwgFIAhAAQArAAAAgkQAAgjgrAAIghAAg");
	this.shape_13.setTransform(226.2,72.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_14.setTransform(210.4,72.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQASAAAMgJQALgIAAgPQABgOgKgKQgHgIgSgHIgSgIQgngQAAgiQAAgTARgNQAOgLAVAAQAfAAAUAXIgMALQgQgTgYAAQgPAAgJAIQgLAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgQANQgQAOgaAAQgSAAgQgIg");
	this.shape_15.setTransform(195.3,72.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIANgMQARAWAbAAQASAAAMgJQAMgIAAgPQgBgOgIgKQgIgIgRgHIgTgIQgngQAAgiQAAgTAQgNQAQgLATAAQAgAAAUAXIgMALQgQgTgZAAQgNAAgLAIQgKAIAAANQAAAXAcAMIATAIQAtASAAAhQAAAWgPANQgRAOgaAAQgRAAgRgIg");
	this.shape_16.setTransform(181.3,72.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_17.setTransform(167.8,72.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("Ag9BcIAAi3IA4AAQAaAAAPAOQAOANgBATQABAcgXAMQAQAFAIAKQALANAAATQAAAYgTAOQgSAMgfAAgAgtBNIAuAAQAVAAANgKQAKgJABgRQgBgRgKgKQgNgKgVAAIguAAgAgtgKIArAAQAQAAAKgJQAKgJgBgPQABgOgKgKQgLgJgPAAIgrAAg");
	this.shape_18.setTransform(152.3,72.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgzBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAkAAAaATIgKANQgWgRgeAAQggAAgWAXQgWAWAAAhQAAAiAWAWQAWAXAgAAQAbAAAVgLIAAg/Ig2AAIAHgPIA/AAIAABWQgbASglAAQgnAAgbgbg");
	this.shape_19.setTransform(126.9,72.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_20.setTransform(114,72.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_21.setTransform(99.9,72.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_22.setTransform(82.8,72.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAAg0IgyCSIg+i5IAQAAIAvCNIAxiPIAyCPIAviNIAQAAIg+C5g");
	this.shape_23.setTransform(63.1,72.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_24.setTransform(34.1,72.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_25.setTransform(20,72.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_26.setTransform(9.8,72.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgzBDQgbgbAAgoQAAgnAbgbQAcgbAnAAQAiAAAbATIgJANQgXgRgdAAQghAAgWAXQgXAWAAAhQAAAiAXAWQAWAXAhAAQAaAAAVgLIAAg/Ig2AAIAHgPIA/AAIAABWQgcASgjAAQgnAAgcgbg");
	this.shape_27.setTransform(412.7,45.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_28.setTransform(394.4,45.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgHBcIAAioIg0AAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_29.setTransform(378.9,45.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_30.setTransform(355.1,45.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_31.setTransform(338.1,45.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AhLBcIAAi3IAzAAQArAAAdAaQAcAaAAAnQAAAogcAaQgdAagrAAgAg7BNIAjAAQAlAAAXgWQAXgVAAgiQAAghgXgVQgXgWglAAIgjAAg");
	this.shape_32.setTransform(321.1,45.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_33.setTransform(304.2,45.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgjBQQgPgOAAgXIAQAAQABARAJAJQAKAJAOAAQAQAAAKgJQAJgJAAgRIAAiHIAQAAIAACJQAAAXgPANQgOAMgWAAQgVAAgOgNg");
	this.shape_34.setTransform(288.5,45.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_35.setTransform(269.5,45.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgyBcIAAi3IARAAIAACoIBTAAIAAAPg");
	this.shape_36.setTransform(257.3,45.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_37.setTransform(242.8,45.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AAAg0IgyCSIg+i5IAQAAIAvCNIAxiPIAyCPIAviNIAQAAIg+C5g");
	this.shape_38.setTransform(223.1,45.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_39.setTransform(198,45.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_40.setTransform(186.4,45.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AhLBcIAAi3IAzAAQArAAAdAaQAcAaAAAnQAAAogcAaQgdAagrAAgAg7BNIAjAAQAlAAAXgWQAXgVAAgiQAAghgXgVQgXgWglAAIgjAAg");
	this.shape_41.setTransform(173.8,45.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("ABJBdIAAiQIhJBSIhIhSIAACQIgQAAIAAi5IBYBkIBZhkIAAC5g");
	this.shape_42.setTransform(145.1,45.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_43.setTransform(124.5,45.6);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIANgMQARAWAbAAQASAAAMgJQAMgIAAgPQgBgOgIgKQgIgIgRgHIgTgIQgngQAAgiQAAgTAQgNQAQgLATAAQAgAAAUAXIgMALQgQgTgZAAQgNAAgLAIQgKAIAAANQAAAXAcAMIATAIQAtASAAAhQAAAWgPANQgRAOgaAAQgRAAgRgIg");
	this.shape_44.setTransform(108.4,45.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_45.setTransform(91,45.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_46.setTransform(77,45.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_47.setTransform(66.8,45.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("ABJBdIAAiQIhJBSIhIhSIAACQIgQAAIAAi5IBYBkIBZhkIAAC5g");
	this.shape_48.setTransform(47.2,45.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_49.setTransform(28.9,45.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgzBDQgbgbAAgoQAAgnAbgbQAcgbAnAAQAjAAAZATIgJANQgVgRgeAAQghAAgWAXQgXAWAAAhQAAAiAXAWQAWAXAhAAQAbAAAUgLIAAg/Ig2AAIAHgPIA/AAIAABWQgcASgjAAQgnAAgcgbg");
	this.shape_50.setTransform(11.2,45.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AAsBcIhEhTIgXAAIAABTIgQAAIAAi3IAwAAQAcAAAPANQAQANAAAYQABAWgNANQgOAMgXADIBFBTgAgvgFIAgAAQAqAAAAgkQAAgjgqAAIggAAg");
	this.shape_51.setTransform(371.8,18.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_52.setTransform(358.9,18.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AAAg0IgyCSIg+i5IAQAAIAvCNIAxiPIAyCPIAviNIAQAAIg+C5g");
	this.shape_53.setTransform(343.5,18.8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIANgMQARAWAbAAQASAAAMgJQALgIAAgPQAAgOgJgKQgHgIgRgHIgTgIQgngQAAgiQAAgTARgNQAOgLAVAAQAfAAAUAXIgMALQgQgTgYAAQgOAAgLAIQgKAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgQANQgPAOgbAAQgRAAgRgIg");
	this.shape_54.setTransform(317.9,18.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQATAAALgJQAMgIAAgPQgBgOgIgKQgIgIgSgHIgSgIQgngQAAgiQAAgTAQgNQAPgLAUAAQAgAAAUAXIgMALQgQgTgZAAQgOAAgKAIQgKAIAAANQAAAXAcAMIATAIQAtASAAAhQAAAWgPANQgQAOgbAAQgRAAgRgIg");
	this.shape_55.setTransform(303.9,18.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_56.setTransform(288.1,18.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AhLBcIAAi3IAzAAQArAAAdAaQAcAaAAAnQAAAogcAaQgdAagrAAgAg7BNIAjAAQAlAAAXgWQAXgVAAgiQAAghgXgVQgXgWglAAIgjAAg");
	this.shape_57.setTransform(270.1,18.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgMAfIAMgjIgLAAIAAgbIAYAAIAAAcIgNAig");
	this.shape_58.setTransform(249.7,28.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_59.setTransform(236,18.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_60.setTransform(218.9,18.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQASAAAMgJQALgIAAgPQABgOgKgKQgHgIgSgHIgSgIQgngQAAgiQAAgTARgNQAOgLAVAAQAfAAAUAXIgMALQgQgTgYAAQgPAAgJAIQgLAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgQANQgQAOgaAAQgSAAgQgIg");
	this.shape_61.setTransform(203.9,18.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIANgMQARAWAbAAQASAAAMgJQAMgIAAgPQgBgOgIgKQgIgIgRgHIgTgIQgngQAAgiQAAgTAQgNQAQgLATAAQAgAAAUAXIgMALQgQgTgZAAQgNAAgLAIQgKAIAAANQAAAXAcAMIATAIQAtASAAAhQAAAWgPANQgRAOgaAAQgRAAgRgIg");
	this.shape_62.setTransform(189.9,18.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_63.setTransform(179.4,18.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AAAg0IgyCSIg+i5IAQAAIAvCNIAxiPIAyCPIAviNIAQAAIg+C5g");
	this.shape_64.setTransform(164,18.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIANgMQARAWAbAAQASAAAMgJQALgIAAgPQAAgOgJgKQgHgIgRgHIgTgIQgngQAAgiQAAgTARgNQAOgLAVAAQAfAAAUAXIgMALQgQgTgYAAQgOAAgLAIQgKAIAAANQAAAXAcAMIATAIQAtASAAAhQAAAWgQANQgPAOgbAAQgRAAgRgIg");
	this.shape_65.setTransform(138.4,18.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_66.setTransform(122.5,18.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhLBcIAAi3IAzAAQArAAAdAaQAcAaAAAnQAAAogcAaQgdAagrAAgAg7BNIAjAAQAlAAAXgWQAXgVAAgiQAAghgXgVQgXgWglAAIgjAAg");
	this.shape_67.setTransform(104.6,18.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AhLBcIAAi3IAzAAQArAAAdAaQAcAaAAAnQAAAogcAaQgdAagrAAgAg7BNIAjAAQAlAAAXgWQAXgVAAgiQAAghgXgVQgXgWglAAIgjAAg");
	this.shape_68.setTransform(78.5,18.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_69.setTransform(57.7,18.8);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgzBLQgVgTAAghIAAhzIARAAIAAB0QgBAaARAOQAOAOAZAAQAaAAAPgOQAPgOAAgaIAAh0IAQAAIAABzQABAhgVATQgTASghAAQggAAgTgSg");
	this.shape_70.setTransform(37.4,18.9);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AgKALQgEgFAAgFQAAgHAEgEQAFgEAFAAQAHAAAEAEQAEAEAAAHQAAAFgEAFQgEAFgHgBQgFABgFgFg");
	this.shape_71.setTransform(17.7,26.7);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AgKALQgEgFAAgFQAAgHAEgEQAFgEAFAAQAHAAAEAEQAEAEAAAHQAAAFgEAFQgEAFgHgBQgFABgFgFg");
	this.shape_72.setTransform(11.4,26.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AgKALQgEgFAAgFQAAgHAEgEQAFgEAFAAQAHAAAEAEQAEAEAAAHQAAAFgEAFQgEAFgHgBQgFABgFgFg");
	this.shape_73.setTransform(5,26.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AAsBcIhDhTIgZAAIAABTIgQAAIAAi3IAyAAQAbAAAQANQAQANAAAYQAAAWgOANQgNAMgXADIBGBTgAgwgFIAhAAQArAAAAgkQAAgjgrAAIghAAg");
	this.shape_74.setTransform(405.1,72.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_75.setTransform(361.6,72.7);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("Ag9BcIAAi3IA5AAQAZAAAOAOQAOANABATQAAAcgXAMQAPAFAJAKQALANAAATQAAAYgTAOQgSAMgeAAgAgtBNIAtAAQAWAAAMgKQALgJAAgRQAAgRgLgKQgMgKgWAAIgtAAgAgtgKIArAAQAQAAAKgJQAJgJAAgPQAAgOgJgKQgKgJgQAAIgrAAg");
	this.shape_76.setTransform(332.9,72.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AgyBcIAAi3IAQAAIAACoIBVAAIAAAPg");
	this.shape_77.setTransform(297.1,72.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgIBcIAAioIg0AAIAAgPIB5AAIAAAPIg1AAIAACog");
	this.shape_78.setTransform(282.7,72.7);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AgIBcIAAioIg0AAIAAgPIB5AAIAAAPIg1AAIAACog");
	this.shape_79.setTransform(269.5,72.7);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AgxBcIAAi3IAQAAIAACoIBTAAIAAAPg");
	this.shape_80.setTransform(250.1,72.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AgHBcIAAhOIhDhpIASAAIA4BbIA5hbIASAAIhDBpIAABOg");
	this.shape_81.setTransform(202.9,72.7);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AgxBDQgbgbAAgoQAAgnAbgbQAcgbAnAAQAiAAAYAUIgJANQgVgSgcAAQghAAgVAXQgXAWAAAhQAAAiAXAWQAVAXAhAAQAcAAAWgSIAIANQgYAUgiAAQgnAAgcgbg");
	this.shape_82.setTransform(122.3,72.7);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AgxBcIAAi3IAPAAIAACoIBVAAIAAAPg");
	this.shape_83.setTransform(358.9,45.7);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_84.setTransform(296.6,45.6);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("AgwBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAiAAAZAUIgJANQgVgSgdAAQggAAgWAXQgXAWAAAhQAAAiAXAWQAWAXAgAAQAcAAAWgSIAJANQgZAUgiAAQgnAAgbgbg");
	this.shape_85.setTransform(279.1,45.7);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AgMAgIAMgjIgLAAIAAgbIAYAAIAAAbIgNAjg");
	this.shape_86.setTransform(215.1,55.4);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AAsBcIhDhTIgZAAIAABTIgQAAIAAi3IAyAAQAbAAAQANQAPANAAAYQAAAWgMANQgOAMgWADIBFBTgAgwgFIAhAAQAqAAAAgkQAAgjgqAAIghAAg");
	this.shape_87.setTransform(205.1,45.7);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("AA5BcIAAhXIhxAAIAABXIgQAAIAAi3IAQAAIAABRIBxAAIAAhRIAQAAIAAC3g");
	this.shape_88.setTransform(171.3,45.7);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_89.setTransform(154.6,45.7);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AgzBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAkAAAZATIgIANQgXgRgeAAQggAAgWAXQgWAWAAAhQAAAiAWAWQAWAXAgAAQAbAAAVgLIAAg/Ig2AAIAHgPIA/AAIAABWQgbASglAAQgnAAgbgbg");
	this.shape_90.setTransform(123.8,45.7);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AhCBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAoAAAbAbQAbAbAAAnQAAAogbAbQgbAbgoAAQgnAAgbgbgAg2g3QgXAWAAAhQAAAiAXAWQAWAXAgAAQAhAAAXgXQAWgWAAgiQAAghgWgWQgXgXghAAQggAAgWAXg");
	this.shape_91.setTransform(103.5,45.7);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AgIBcIAAioIg0AAIAAgPIB5AAIAAAPIg2AAIAACog");
	this.shape_92.setTransform(86.1,45.7);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AgMAgIAMgjIgLAAIAAgbIAYAAIAAAbIgNAjg");
	this.shape_93.setTransform(69.5,55.4);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_94.setTransform(59.7,45.7);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_95.setTransform(44.1,45.6);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("AA5BcIAAhXIhxAAIAABXIgQAAIAAi3IAQAAIAABRIBxAAIAAhRIAQAAIAAC3g");
	this.shape_96.setTransform(25.1,45.7);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_97.setTransform(8.5,45.7);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("AgzBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAjAAAaATIgJANQgVgRgfAAQggAAgWAXQgXAWABAhQgBAiAXAWQAWAXAgAAQAcAAAUgLIAAg/Ig2AAIAHgPIA/AAIAABWQgbASglAAQgnAAgbgbg");
	this.shape_98.setTransform(286.4,18.8);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFFFFF").s().p("AgyBcIAAi3IAQAAIAACoIBVAAIAAAPg");
	this.shape_99.setTransform(237.6,18.8);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFFFF").s().p("AhCBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAoAAAbAbQAbAbAAAnQAAAogbAbQgbAbgoAAQgnAAgbgbgAg2g3QgXAWAAAhQAAAiAXAWQAWAXAgAAQAhAAAXgXQAWgWAAgiQAAghgWgWQgXgXghAAQggAAgWAXg");
	this.shape_100.setTransform(195.2,18.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFFFFF").s().p("AAtBcIhghYIAABYIgQAAIAAi3IAQAAIAABbIBbhbIAVAAIhcBdIBjBag");
	this.shape_101.setTransform(156.6,18.8);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFFFFF").s().p("AA5BcIAAhXIhxAAIAABXIgQAAIAAi3IAQAAIAABRIBxAAIAAhRIAQAAIAAC3g");
	this.shape_102.setTransform(115.8,18.8);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_103.setTransform(36.6,18.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66,p:{x:122.5,y:18.7}},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57,p:{x:270.1}},{t:this.shape_56,p:{x:288.1,y:18.7}},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47,p:{x:66.8,y:45.7}},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43,p:{x:124.5,y:45.6}},{t:this.shape_42},{t:this.shape_41,p:{x:173.8,y:45.7}},{t:this.shape_40},{t:this.shape_39,p:{x:198,y:45.7}},{t:this.shape_38,p:{x:223.1,y:45.7}},{t:this.shape_37,p:{x:242.8,y:45.7}},{t:this.shape_36,p:{x:257.3}},{t:this.shape_35,p:{x:269.5,y:45.7}},{t:this.shape_34},{t:this.shape_33,p:{x:304.2}},{t:this.shape_32,p:{x:321.1,y:45.7}},{t:this.shape_31,p:{x:338.1}},{t:this.shape_30},{t:this.shape_29,p:{x:378.9,y:45.7}},{t:this.shape_28,p:{x:394.4,y:45.6}},{t:this.shape_27},{t:this.shape_26,p:{x:9.8,y:72.7}},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23,p:{x:63.1,y:72.7}},{t:this.shape_22,p:{x:82.8}},{t:this.shape_21},{t:this.shape_20,p:{x:114}},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17,p:{x:167.8}},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14,p:{x:210.4}},{t:this.shape_13},{t:this.shape_12,p:{x:251.3}},{t:this.shape_11,p:{x:271.9}},{t:this.shape_10},{t:this.shape_9,p:{x:308.6}},{t:this.shape_8,p:{x:326.6}},{t:this.shape_7},{t:this.shape_6,p:{x:370.4}},{t:this.shape_5},{t:this.shape_4,p:{x:409.4,y:72.7}},{t:this.shape_3,p:{x:430.4,y:72.7}},{t:this.shape_2,p:{x:448.8}},{t:this.shape_1,p:{x:465.9,y:72.7}},{t:this.shape,p:{x:479.5}}]},1).to({state:[{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_103},{t:this.shape_4,p:{x:56,y:18.8}},{t:this.shape_57,p:{x:76.8}},{t:this.shape_29,p:{x:99.1,y:18.8}},{t:this.shape_102},{t:this.shape_47,p:{x:133.8,y:18.8}},{t:this.shape_101},{t:this.shape_3,p:{x:173.9,y:18.8}},{t:this.shape_100},{t:this.shape_38,p:{x:217.9,y:18.8}},{t:this.shape_99},{t:this.shape_39,p:{x:251.1,y:18.8}},{t:this.shape_41,p:{x:268.1,y:18.8}},{t:this.shape_98},{t:this.shape_37,p:{x:304.1,y:18.8}},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_33,p:{x:141.4}},{t:this.shape_89},{t:this.shape_88},{t:this.shape_31,p:{x:189.3}},{t:this.shape_87},{t:this.shape_86},{t:this.shape_23,p:{x:236.9,y:45.7}},{t:this.shape_26,p:{x:256.7,y:45.7}},{t:this.shape_85},{t:this.shape_84},{t:this.shape_1,p:{x:316,y:45.7}},{t:this.shape_66,p:{x:342.1,y:45.6}},{t:this.shape_83},{t:this.shape_36,p:{x:372.4}},{t:this.shape_12,p:{x:13.6}},{t:this.shape_56,p:{x:34.2,y:72.6}},{t:this.shape_6,p:{x:52.7}},{t:this.shape_22,p:{x:67.4}},{t:this.shape_17,p:{x:88.7}},{t:this.shape_43,p:{x:104.2,y:72.6}},{t:this.shape_82},{t:this.shape_9,p:{x:140.9}},{t:this.shape_32,p:{x:168,y:72.7}},{t:this.shape_28,p:{x:186,y:72.6}},{t:this.shape_81},{t:this.shape_11,p:{x:226.6}},{t:this.shape_80},{t:this.shape_20,p:{x:259.3}},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_14,p:{x:310.6}},{t:this.shape_76},{t:this.shape_8,p:{x:348.4}},{t:this.shape_75},{t:this.shape_35,p:{x:374.8,y:72.7}},{t:this.shape_2,p:{x:389.3}},{t:this.shape_74},{t:this.shape,p:{x:415}}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.flake = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Ebene 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0.8)","rgba(110,207,255,0)"],[0,1],0,0,0,0,0,7.9).s().p("AgyAzQgWgVAAgeQAAgdAWgVQAVgWAdAAQAeAAAVAWQAVAVABAdQgBAegVAVQgVAVgeABQgdgBgVgVg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.2,-7.2,14.5,14.5);


(lib.flame = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_15 = function() {
		this.gotoAndPlay(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(15).call(this.frame_15).wait(1));

	// light
	this.instance = new lib.light("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.91,y:-3.5},2).to({scaleX:1.31,scaleY:0.83,y:2.3},2).to({scaleX:1.39,scaleY:1.39,y:0},1).to({regX:0.1,regY:0.1,scaleX:0.96,scaleY:0.8,x:1.9,y:2.4},2).to({regX:0,regY:0,scaleX:0.51,scaleY:0.83,x:-0.7,y:-4},4).to({scaleX:0.93,scaleY:0.8,x:-2.5,y:1.1},2).to({scaleX:1,scaleY:1,x:0,y:0},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30.7,-30.7,61.5,61.5);


(lib.flakes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Ebene 1
	this.instance = new lib.flake("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(393,187.1,0.644,0.644,0,0,0,0,0.1);
	this.instance.alpha = 0.5;

	this.instance_1 = new lib.flake("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-424.8,134.5,0.644,0.644);
	this.instance_1.alpha = 0.5;

	this.instance_2 = new lib.flake("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(298.3,-200.3,0.644,0.644,0,0,0,0.1,0.1);
	this.instance_2.alpha = 0.5;

	this.instance_3 = new lib.flake("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(262.5,65.7,0.644,0.644,0,0,0,0.1,0);

	this.instance_4 = new lib.flake("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(110.8,-109.5,0.644,0.644,0,0,0,0.1,0);
	this.instance_4.alpha = 0.5;

	this.instance_5 = new lib.flake("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-101.8,247.8,0.644,0.644,0,0,0,0.1,0.1);

	this.instance_6 = new lib.flake("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(-98.2,151.1,0.644,0.644,0,0,0,0.1,0);

	this.instance_7 = new lib.flake("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(-205.5,-41.3,0.644,0.644,0,0,0,0,0.1);

	this.instance_8 = new lib.flake("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(-111.1,-125.7,0.644,0.644,0,0,0,0,0.1);
	this.instance_8.alpha = 0.5;

	this.instance_9 = new lib.flake("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(-379.3,-214.9,0.644,0.644);

	this.instance_10 = new lib.flake("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(-367.4,250.4);
	this.instance_10.alpha = 0.75;

	this.instance_11 = new lib.flake("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(580.2,273.7);

	this.instance_12 = new lib.flake("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(-470.8,20.7);

	this.instance_13 = new lib.flake("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(-266.3,60,1.44,1.44);
	this.instance_13.alpha = 0.5;

	this.instance_14 = new lib.flake("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(-341.1,-283.7,1.44,1.44);
	this.instance_14.alpha = 0.5;

	this.instance_15 = new lib.flake("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(386.2,10,1.44,1.44);
	this.instance_15.alpha = 0.5;

	this.instance_16 = new lib.flake("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(594.7,-187.1);

	this.instance_17 = new lib.flake("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(262.7,-291.3,1.44,1.44);
	this.instance_17.alpha = 0.75;

	this.instance_18 = new lib.flake("synched",0);
	this.instance_18.parent = this;
	this.instance_18.setTransform(-478.3,-116.8);

	this.instance_19 = new lib.flake("synched",0);
	this.instance_19.parent = this;
	this.instance_19.setTransform(499,-242.3);
	this.instance_19.alpha = 0.5;

	this.instance_20 = new lib.flake("synched",0);
	this.instance_20.parent = this;
	this.instance_20.setTransform(280.4,207.6);
	this.instance_20.alpha = 0.75;

	this.instance_21 = new lib.flake("synched",0);
	this.instance_21.parent = this;
	this.instance_21.setTransform(432.7,253.6,1.44,1.44);

	this.instance_22 = new lib.flake("synched",0);
	this.instance_22.parent = this;
	this.instance_22.setTransform(450.4,-147.8);
	this.instance_22.alpha = 0.75;

	this.instance_23 = new lib.flake("synched",0);
	this.instance_23.parent = this;
	this.instance_23.setTransform(377.5,85.1);

	this.instance_24 = new lib.flake("synched",0);
	this.instance_24.parent = this;
	this.instance_24.setTransform(225.8,-76.8);

	this.instance_25 = new lib.flake("synched",0);
	this.instance_25.parent = this;
	this.instance_25.setTransform(524,95.4,1.44,1.44);

	this.instance_26 = new lib.flake("synched",0);
	this.instance_26.parent = this;
	this.instance_26.setTransform(141.8,-187.1);

	this.instance_27 = new lib.flake("synched",0);
	this.instance_27.parent = this;
	this.instance_27.setTransform(11.8,-17.1);

	this.instance_28 = new lib.flake("synched",0);
	this.instance_28.parent = this;
	this.instance_28.setTransform(207.1,25,1.44,1.44);

	this.instance_29 = new lib.flake("synched",0);
	this.instance_29.parent = this;
	this.instance_29.setTransform(152.5,291.4,1.44,1.44);
	this.instance_29.alpha = 0.5;

	this.instance_30 = new lib.flake("synched",0);
	this.instance_30.parent = this;
	this.instance_30.setTransform(71.8,182.9);
	this.instance_30.alpha = 0.5;

	this.instance_31 = new lib.flake("synched",0);
	this.instance_31.parent = this;
	this.instance_31.setTransform(37.1,167.7);

	this.instance_32 = new lib.flake("synched",0);
	this.instance_32.parent = this;
	this.instance_32.setTransform(77.1,77.7);
	this.instance_32.alpha = 0.5;

	this.instance_33 = new lib.flake("synched",0);
	this.instance_33.parent = this;
	this.instance_33.setTransform(56.4,-184,1.44,1.44);
	this.instance_33.alpha = 0.5;

	this.instance_34 = new lib.flake("synched",0);
	this.instance_34.parent = this;
	this.instance_34.setTransform(-22.9,-162.3);
	this.instance_34.alpha = 0.75;

	this.instance_35 = new lib.flake("synched",0);
	this.instance_35.parent = this;
	this.instance_35.setTransform(-102,-245.5,1.44,1.44);
	this.instance_35.alpha = 0.75;

	this.instance_36 = new lib.flake("synched",0);
	this.instance_36.parent = this;
	this.instance_36.setTransform(-116.4,89.5,1.44,1.44);
	this.instance_36.alpha = 0.75;

	this.instance_37 = new lib.flake("synched",0);
	this.instance_37.parent = this;
	this.instance_37.setTransform(-182.9,217.7);

	this.instance_38 = new lib.flake("synched",0);
	this.instance_38.parent = this;
	this.instance_38.setTransform(-302.9,177.7);

	this.instance_39 = new lib.flake("synched",0);
	this.instance_39.parent = this;
	this.instance_39.setTransform(-372.9,27.7);

	this.instance_40 = new lib.flake("synched",0);
	this.instance_40.parent = this;
	this.instance_40.setTransform(-591.4,75.1,1.44,1.44);
	this.instance_40.alpha = 0.75;

	this.instance_41 = new lib.flake("synched",0);
	this.instance_41.parent = this;
	this.instance_41.setTransform(-412.9,-242.3);

	this.instance_42 = new lib.flake("synched",0);
	this.instance_42.parent = this;
	this.instance_42.setTransform(-505,-140.8,1.44,1.44);
	this.instance_42.alpha = 0.5;

	this.instance_43 = new lib.flake("synched",0);
	this.instance_43.parent = this;
	this.instance_43.setTransform(-172.9,-62.3);

	this.instance_44 = new lib.flake("synched",0);
	this.instance_44.parent = this;
	this.instance_44.setTransform(-202.9,-212.3);

	this.instance_45 = new lib.flake("synched",0);
	this.instance_45.parent = this;
	this.instance_45.setTransform(-292.9,-152.3);
	this.instance_45.alpha = 0.75;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_45},{t:this.instance_44},{t:this.instance_43},{t:this.instance_42},{t:this.instance_41},{t:this.instance_40},{t:this.instance_39},{t:this.instance_38},{t:this.instance_37},{t:this.instance_36},{t:this.instance_35},{t:this.instance_34},{t:this.instance_33},{t:this.instance_32},{t:this.instance_31},{t:this.instance_30},{t:this.instance_29},{t:this.instance_28},{t:this.instance_27},{t:this.instance_26},{t:this.instance_25},{t:this.instance_24},{t:this.instance_23},{t:this.instance_22},{t:this.instance_21},{t:this.instance_20},{t:this.instance_19},{t:this.instance_18},{t:this.instance_17},{t:this.instance_16},{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-601.9,-301.8,1203.9,603.6);


(lib.snow = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_249 = function() {
		this.gotoAndPlay(1);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(249).call(this.frame_249).wait(1));

	// snow
	this.instance = new lib.flakes("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-2.5,-98.3,1.308,1.308,0,180,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({x:-3,y:-91.6},0).wait(1).to({x:-3.5,y:-84.9},0).wait(1).to({x:-4,y:-78.3},0).wait(1).to({x:-4.5,y:-71.6},0).wait(1).to({x:-5,y:-64.9},0).wait(1).to({x:-5.5,y:-58.2},0).wait(1).to({x:-6,y:-51.6},0).wait(1).to({x:-6.5,y:-44.9},0).wait(1).to({x:-7,y:-38.2},0).wait(1).to({x:-7.5,y:-31.5},0).wait(1).to({x:-8,y:-24.9},0).wait(1).to({x:-8.5,y:-18.2},0).wait(1).to({x:-9,y:-11.5},0).wait(1).to({x:-9.5,y:-4.8},0).wait(1).to({x:-10,y:1.8},0).wait(1).to({x:-10.5,y:8.5},0).wait(1).to({x:-11.1,y:15.2},0).wait(1).to({x:-11.6,y:21.9},0).wait(1).to({x:-12.1,y:28.5},0).wait(1).to({x:-12.6,y:35.2},0).wait(1).to({x:-13.2,y:41.9},0).wait(1).to({x:-13.7,y:48.6},0).wait(1).to({x:-14.2,y:55.3},0).wait(1).to({x:-14.8,y:61.9},0).wait(1).to({x:-15.3,y:68.6},0).wait(1).to({x:-15.9,y:75.3},0).wait(1).to({x:-16.5,y:82},0).wait(1).to({x:-17,y:88.6},0).wait(1).to({x:-17.6,y:95.3},0).wait(1).to({x:-18.2,y:102},0).wait(1).to({x:-18.8,y:108.7},0).wait(1).to({x:-19.3,y:115.3},0).wait(1).to({x:-19.9,y:122},0).wait(1).to({x:-20.5,y:128.7},0).wait(1).to({x:-21.2,y:135.4},0).wait(1).to({x:-21.8,y:142},0).wait(1).to({x:-22.4,y:148.7},0).wait(1).to({x:-23,y:155.4},0).wait(1).to({x:-23.7,y:162.1},0).wait(1).to({x:-24.3,y:168.7},0).wait(1).to({x:-24.9,y:175.4},0).wait(1).to({x:-25.6,y:182.1},0).wait(1).to({x:-26.3,y:188.8},0).wait(1).to({x:-26.9,y:195.5},0).wait(1).to({x:-27.6,y:202.1},0).wait(1).to({x:-28.3,y:208.8},0).wait(1).to({x:-29,y:215.5},0).wait(1).to({x:-29.7,y:222.2},0).wait(1).to({x:-30.4,y:228.8},0).wait(1).to({x:-31.2,y:235.5},0).wait(1).to({x:-31.9,y:242.2},0).wait(1).to({x:-32.7,y:248.9},0).wait(1).to({x:-33.4,y:255.5},0).wait(1).to({x:-34.2,y:262.2},0).wait(1).to({x:-35,y:268.9},0).wait(1).to({x:-35.8,y:275.6},0).wait(1).to({x:-36.6,y:282.2},0).wait(1).to({x:-37.4,y:288.9},0).wait(1).to({x:-38.2,y:295.6},0).wait(1).to({x:-39,y:302.3},0).wait(1).to({x:-39.9,y:308.9},0).wait(1).to({x:-40.7,y:315.6},0).wait(1).to({x:-41.6,y:322.3},0).wait(1).to({x:-42.5,y:329},0).wait(1).to({x:-43.4,y:335.7},0).wait(1).to({x:-44.3,y:342.3},0).wait(1).to({x:-45.2,y:349},0).wait(1).to({x:-46.1,y:355.7},0).wait(1).to({x:-47,y:362.4},0).wait(1).to({x:-48,y:369},0).wait(1).to({x:-49,y:375.7},0).wait(1).to({x:-50,y:382.4},0).wait(1).to({x:-50.9,y:389.1},0).wait(1).to({x:-52,y:395.7},0).wait(1).to({x:-53,y:402.4},0).wait(1).to({x:-54,y:409.1},0).wait(1).to({x:-55.1,y:415.8},0).wait(1).to({x:-56.1,y:422.4},0).wait(1).to({x:-57.2,y:429.1},0).wait(1).to({x:-58.3,y:435.8},0).wait(1).to({x:-59.4,y:442.5},0).wait(1).to({x:-60.6,y:449.1},0).wait(1).to({x:-61.7,y:455.8},0).wait(1).to({x:-62.9,y:462.5},0).wait(1).to({x:-64.1,y:469.2},0).wait(1).to({x:-65.2,y:475.9},0).wait(1).to({x:-66.5,y:482.5},0).wait(1).to({x:-67.7,y:489.2},0).wait(1).to({x:-68.9,y:495.9},0).wait(1).to({x:-70.2,y:502.6},0).wait(1).to({x:-71.5,y:509.2},0).wait(1).to({x:-72.8,y:515.9},0).wait(1).to({x:-74.1,y:522.6},0).wait(1).to({x:-75.4,y:529.3},0).wait(1).to({x:-76.7,y:535.9},0).wait(1).to({x:-78.1,y:542.6},0).wait(1).to({x:-79.5,y:549.3},0).wait(1).to({x:-80.9,y:556},0).wait(1).to({x:-82.3,y:562.6},0).wait(1).to({x:-83.8,y:569.3},0).wait(1).to({x:-85.2,y:576},0).wait(1).to({x:-86.7,y:582.7},0).wait(1).to({x:-88.2,y:589.3},0).wait(1).to({x:-89.7,y:596},0).wait(1).to({x:-91.2,y:602.7},0).wait(1).to({x:-92.8,y:609.4},0).wait(1).to({x:-94.4,y:616},0).wait(1).to({x:-96,y:622.7},0).wait(1).to({x:-97.6,y:629.4},0).wait(1).to({x:-99.2,y:636.1},0).wait(1).to({x:-100.9,y:642.8},0).wait(1).to({x:-102.6,y:649.4},0).wait(1).to({x:-104.3,y:656.1},0).wait(1).to({x:-106,y:662.8},0).wait(1).to({x:-107.8,y:669.5},0).wait(1).to({x:-109.5,y:676.1},0).wait(1).to({x:-111.3,y:682.8},0).wait(1).to({x:-113.1,y:689.5},0).wait(1).to({x:-115,y:696.2},0).wait(1).to({x:-116.8,y:702.8},0).wait(1).to({x:-118.7,y:709.5},0).wait(1).to({x:-120.6,y:716.2},0).wait(1).to({x:-122.5,y:722.9},0).wait(1).to({x:-124,y:-827.3},0).wait(1).to({x:-122.5,y:-821.5},0).wait(1).to({x:-120.6,y:-815.6},0).wait(1).to({x:-118.7,y:-809.8},0).wait(1).to({x:-116.9,y:-804},0).wait(1).to({x:-115,y:-798.1},0).wait(1).to({x:-113.2,y:-792.3},0).wait(1).to({x:-111.4,y:-786.5},0).wait(1).to({x:-109.6,y:-780.6},0).wait(1).to({x:-107.8,y:-774.8},0).wait(1).to({x:-106.1,y:-769},0).wait(1).to({x:-104.4,y:-763.2},0).wait(1).to({x:-102.7,y:-757.3},0).wait(1).to({x:-101,y:-751.5},0).wait(1).to({x:-99.3,y:-745.7},0).wait(1).to({x:-97.7,y:-739.8},0).wait(1).to({x:-96.1,y:-734},0).wait(1).to({x:-94.5,y:-728.2},0).wait(1).to({x:-92.9,y:-722.3},0).wait(1).to({x:-91.4,y:-716.5},0).wait(1).to({x:-89.8,y:-710.7},0).wait(1).to({x:-88.3,y:-704.8},0).wait(1).to({x:-86.8,y:-699},0).wait(1).to({x:-85.3,y:-693.2},0).wait(1).to({x:-83.9,y:-687.3},0).wait(1).to({x:-82.5,y:-681.5},0).wait(1).to({x:-81,y:-675.7},0).wait(1).to({x:-79.6,y:-669.8},0).wait(1).to({x:-78.3,y:-664},0).wait(1).to({x:-76.9,y:-658.2},0).wait(1).to({x:-75.6,y:-652.3},0).wait(1).to({x:-74.2,y:-646.5},0).wait(1).to({x:-72.9,y:-640.7},0).wait(1).to({x:-71.6,y:-634.8},0).wait(1).to({x:-70.4,y:-629},0).wait(1).to({x:-69.1,y:-623.2},0).wait(1).to({x:-67.9,y:-617.3},0).wait(1).to({x:-66.6,y:-611.5},0).wait(1).to({x:-65.4,y:-605.7},0).wait(1).to({x:-64.2,y:-599.9},0).wait(1).to({x:-63.1,y:-594},0).wait(1).to({x:-61.9,y:-588.2},0).wait(1).to({x:-60.8,y:-582.4},0).wait(1).to({x:-59.6,y:-576.5},0).wait(1).to({x:-58.5,y:-570.7},0).wait(1).to({x:-57.4,y:-564.9},0).wait(1).to({x:-56.4,y:-559},0).wait(1).to({x:-55.3,y:-553.2},0).wait(1).to({x:-54.2,y:-547.4},0).wait(1).to({x:-53.2,y:-541.5},0).wait(1).to({x:-52.2,y:-535.7},0).wait(1).to({x:-51.2,y:-529.9},0).wait(1).to({x:-50.2,y:-524},0).wait(1).to({x:-49.2,y:-518.2},0).wait(1).to({x:-48.2,y:-512.4},0).wait(1).to({x:-47.3,y:-506.5},0).wait(1).to({x:-46.3,y:-500.7},0).wait(1).to({x:-45.4,y:-494.9},0).wait(1).to({x:-44.5,y:-489},0).wait(1).to({x:-43.6,y:-483.2},0).wait(1).to({x:-42.7,y:-477.4},0).wait(1).to({x:-41.8,y:-471.5},0).wait(1).to({x:-41,y:-465.7},0).wait(1).to({x:-40.1,y:-459.9},0).wait(1).to({x:-39.3,y:-454.1},0).wait(1).to({x:-38.4,y:-448.2},0).wait(1).to({x:-37.6,y:-442.4},0).wait(1).to({x:-36.8,y:-436.6},0).wait(1).to({x:-36,y:-430.7},0).wait(1).to({x:-35.2,y:-424.9},0).wait(1).to({x:-34.4,y:-419.1},0).wait(1).to({x:-33.7,y:-413.2},0).wait(1).to({x:-32.9,y:-407.4},0).wait(1).to({x:-32.2,y:-401.6},0).wait(1).to({x:-31.4,y:-395.7},0).wait(1).to({x:-30.7,y:-389.9},0).wait(1).to({x:-30,y:-384.1},0).wait(1).to({x:-29.3,y:-378.2},0).wait(1).to({x:-28.6,y:-372.4},0).wait(1).to({x:-27.9,y:-366.6},0).wait(1).to({x:-27.2,y:-360.7},0).wait(1).to({x:-26.6,y:-354.9},0).wait(1).to({x:-25.9,y:-349.1},0).wait(1).to({x:-25.2,y:-343.2},0).wait(1).to({x:-24.6,y:-337.4},0).wait(1).to({x:-23.9,y:-331.6},0).wait(1).to({x:-23.3,y:-325.8},0).wait(1).to({x:-22.7,y:-319.9},0).wait(1).to({x:-22.1,y:-314.1},0).wait(1).to({x:-21.5,y:-308.3},0).wait(1).to({x:-20.9,y:-302.4},0).wait(1).to({x:-20.3,y:-296.6},0).wait(1).to({x:-19.7,y:-290.8},0).wait(1).to({x:-19.1,y:-284.9},0).wait(1).to({x:-18.5,y:-279.1},0).wait(1).to({x:-17.9,y:-273.3},0).wait(1).to({x:-17.4,y:-267.4},0).wait(1).to({x:-16.8,y:-261.6},0).wait(1).to({x:-16.2,y:-255.8},0).wait(1).to({x:-15.7,y:-249.9},0).wait(1).to({x:-15.1,y:-244.1},0).wait(1).to({x:-14.6,y:-238.3},0).wait(1).to({x:-14.1,y:-232.4},0).wait(1).to({x:-13.5,y:-226.6},0).wait(1).to({x:-13,y:-220.8},0).wait(1).to({x:-12.5,y:-214.9},0).wait(1).to({x:-11.9,y:-209.1},0).wait(1).to({x:-11.4,y:-203.3},0).wait(1).to({x:-10.9,y:-197.4},0).wait(1).to({x:-10.4,y:-191.6},0).wait(1).to({x:-9.9,y:-185.8},0).wait(1).to({x:-9.4,y:-179.9},0).wait(1).to({x:-8.9,y:-174.1},0).wait(1).to({x:-8.4,y:-168.3},0).wait(1).to({x:-7.9,y:-162.5},0).wait(1).to({x:-7.4,y:-156.6},0).wait(1).to({x:-6.9,y:-150.8},0).wait(1).to({x:-6.4,y:-145},0).wait(1).to({x:-5.9,y:-139.1},0).wait(1).to({x:-5.4,y:-133.3},0).wait(1).to({x:-4.9,y:-127.5},0).wait(1).to({x:-4.4,y:-121.6},0).wait(1).to({x:-4,y:-115.8},0).wait(1).to({x:-3.5,y:-110},0).wait(1).to({x:-3,y:-104.1},0).wait(1).to({x:-2.5,y:-98.3},0).wait(1));

	// snow
	this.instance_1 = new lib.flakes("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(18.8,-792,1.308,1.308,180,0,0,-0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:0,regY:0,x:18.6,y:-786},0).wait(1).to({y:-780.2},0).wait(1).to({y:-774.3},0).wait(1).to({y:-768.5},0).wait(1).to({y:-762.6},0).wait(1).to({y:-756.8},0).wait(1).to({y:-750.9},0).wait(1).to({y:-745.1},0).wait(1).to({y:-739.2},0).wait(1).to({y:-733.3},0).wait(1).to({y:-727.5},0).wait(1).to({y:-721.6},0).wait(1).to({y:-715.8},0).wait(1).to({y:-709.9},0).wait(1).to({y:-704.1},0).wait(1).to({y:-698.2},0).wait(1).to({y:-692.4},0).wait(1).to({y:-686.5},0).wait(1).to({y:-680.6},0).wait(1).to({y:-674.8},0).wait(1).to({y:-668.9},0).wait(1).to({y:-663.1},0).wait(1).to({y:-657.2},0).wait(1).to({y:-651.4},0).wait(1).to({y:-645.5},0).wait(1).to({y:-639.7},0).wait(1).to({y:-633.8},0).wait(1).to({y:-627.9},0).wait(1).to({y:-622.1},0).wait(1).to({y:-616.2},0).wait(1).to({y:-610.4},0).wait(1).to({y:-604.5},0).wait(1).to({y:-598.7},0).wait(1).to({y:-592.8},0).wait(1).to({y:-587},0).wait(1).to({y:-581.1},0).wait(1).to({y:-575.2},0).wait(1).to({y:-569.4},0).wait(1).to({y:-563.5},0).wait(1).to({y:-557.7},0).wait(1).to({y:-551.8},0).wait(1).to({y:-546},0).wait(1).to({y:-540.1},0).wait(1).to({y:-534.3},0).wait(1).to({y:-528.4},0).wait(1).to({y:-522.6},0).wait(1).to({y:-516.7},0).wait(1).to({y:-510.8},0).wait(1).to({y:-505},0).wait(1).to({y:-499.1},0).wait(1).to({y:-493.3},0).wait(1).to({y:-487.4},0).wait(1).to({y:-481.6},0).wait(1).to({y:-475.7},0).wait(1).to({y:-469.9},0).wait(1).to({y:-464},0).wait(1).to({y:-458.1},0).wait(1).to({y:-452.3},0).wait(1).to({y:-446.4},0).wait(1).to({y:-440.6},0).wait(1).to({y:-434.7},0).wait(1).to({y:-428.9},0).wait(1).to({y:-423},0).wait(1).to({y:-417.2},0).wait(1).to({y:-411.3},0).wait(1).to({y:-405.4},0).wait(1).to({y:-399.6},0).wait(1).to({y:-393.7},0).wait(1).to({y:-387.9},0).wait(1).to({y:-382},0).wait(1).to({y:-376.2},0).wait(1).to({y:-370.3},0).wait(1).to({y:-364.5},0).wait(1).to({y:-358.6},0).wait(1).to({y:-352.7},0).wait(1).to({y:-346.9},0).wait(1).to({y:-341},0).wait(1).to({y:-335.2},0).wait(1).to({y:-329.3},0).wait(1).to({y:-323.5},0).wait(1).to({y:-317.6},0).wait(1).to({y:-311.8},0).wait(1).to({y:-305.9},0).wait(1).to({y:-300},0).wait(1).to({y:-294.2},0).wait(1).to({y:-288.3},0).wait(1).to({y:-282.5},0).wait(1).to({y:-276.6},0).wait(1).to({y:-270.8},0).wait(1).to({y:-264.9},0).wait(1).to({y:-259.1},0).wait(1).to({y:-253.2},0).wait(1).to({y:-247.3},0).wait(1).to({y:-241.5},0).wait(1).to({y:-235.6},0).wait(1).to({y:-229.8},0).wait(1).to({y:-223.9},0).wait(1).to({y:-218.1},0).wait(1).to({y:-212.2},0).wait(1).to({y:-206.4},0).wait(1).to({y:-200.5},0).wait(1).to({y:-194.6},0).wait(1).to({y:-188.8},0).wait(1).to({y:-182.9},0).wait(1).to({y:-177.1},0).wait(1).to({y:-171.2},0).wait(1).to({y:-165.4},0).wait(1).to({y:-159.5},0).wait(1).to({y:-153.7},0).wait(1).to({y:-147.8},0).wait(1).to({y:-141.9},0).wait(1).to({y:-136.1},0).wait(1).to({y:-130.2},0).wait(1).to({y:-124.4},0).wait(1).to({y:-118.5},0).wait(1).to({y:-112.7},0).wait(1).to({y:-106.8},0).wait(1).to({y:-101},0).wait(1).to({y:-95.1},0).wait(1).to({y:-89.2},0).wait(1).to({y:-83.4},0).wait(1).to({y:-77.5},0).wait(1).to({y:-71.7},0).wait(1).to({y:-65.8},0).wait(1).to({y:-60},0).wait(1).to({y:-54.1},0).wait(1).to({y:-48.3},0).wait(1).to({y:-42.4},0).wait(1).to({y:-36.6},0).wait(1).to({y:-30.7},0).wait(1).to({y:-24.8},0).wait(1).to({y:-19},0).wait(1).to({y:-13.1},0).wait(1).to({y:-7.3},0).wait(1).to({y:-1.4},0).wait(1).to({y:4.4},0).wait(1).to({y:10.3},0).wait(1).to({y:16.1},0).wait(1).to({y:22},0).wait(1).to({y:27.9},0).wait(1).to({y:33.7},0).wait(1).to({y:39.6},0).wait(1).to({y:45.4},0).wait(1).to({y:51.3},0).wait(1).to({y:57.1},0).wait(1).to({y:63},0).wait(1).to({y:68.8},0).wait(1).to({y:74.7},0).wait(1).to({y:80.6},0).wait(1).to({y:86.4},0).wait(1).to({y:92.3},0).wait(1).to({y:98.1},0).wait(1).to({y:104},0).wait(1).to({y:109.8},0).wait(1).to({y:115.7},0).wait(1).to({y:121.5},0).wait(1).to({y:127.4},0).wait(1).to({y:133.3},0).wait(1).to({y:139.1},0).wait(1).to({y:145},0).wait(1).to({y:150.8},0).wait(1).to({y:156.7},0).wait(1).to({y:162.5},0).wait(1).to({y:168.4},0).wait(1).to({y:174.2},0).wait(1).to({y:180.1},0).wait(1).to({y:186},0).wait(1).to({y:191.8},0).wait(1).to({y:197.7},0).wait(1).to({y:203.5},0).wait(1).to({y:209.4},0).wait(1).to({y:215.2},0).wait(1).to({y:221.1},0).wait(1).to({y:226.9},0).wait(1).to({y:232.8},0).wait(1).to({y:238.7},0).wait(1).to({y:244.5},0).wait(1).to({y:250.4},0).wait(1).to({y:256.2},0).wait(1).to({y:262.1},0).wait(1).to({y:267.9},0).wait(1).to({y:273.8},0).wait(1).to({y:279.6},0).wait(1).to({y:285.5},0).wait(1).to({y:291.4},0).wait(1).to({y:297.2},0).wait(1).to({y:303.1},0).wait(1).to({y:308.9},0).wait(1).to({y:314.8},0).wait(1).to({y:320.6},0).wait(1).to({y:326.5},0).wait(1).to({y:332.3},0).wait(1).to({y:338.2},0).wait(1).to({y:344.1},0).wait(1).to({y:349.9},0).wait(1).to({y:355.8},0).wait(1).to({y:361.6},0).wait(1).to({y:367.5},0).wait(1).to({y:373.3},0).wait(1).to({y:379.2},0).wait(1).to({y:385},0).wait(1).to({y:390.9},0).wait(1).to({y:396.8},0).wait(1).to({y:402.6},0).wait(1).to({y:408.5},0).wait(1).to({y:414.3},0).wait(1).to({y:420.2},0).wait(1).to({y:426},0).wait(1).to({y:431.9},0).wait(1).to({y:437.7},0).wait(1).to({y:443.6},0).wait(1).to({y:449.4},0).wait(1).to({y:455.3},0).wait(1).to({y:461.2},0).wait(1).to({y:467},0).wait(1).to({y:472.9},0).wait(1).to({y:478.7},0).wait(1).to({y:484.6},0).wait(1).to({y:490.4},0).wait(1).to({y:496.3},0).wait(1).to({y:502.1},0).wait(1).to({y:508},0).wait(1).to({y:513.9},0).wait(1).to({y:519.7},0).wait(1).to({y:525.6},0).wait(1).to({y:531.4},0).wait(1).to({y:537.3},0).wait(1).to({y:543.1},0).wait(1).to({y:549},0).wait(1).to({y:554.8},0).wait(1).to({y:560.7},0).wait(1).to({y:566.6},0).wait(1).to({y:572.4},0).wait(1).to({y:578.3},0).wait(1).to({y:584.1},0).wait(1).to({y:590},0).wait(1).to({y:595.8},0).wait(1).to({y:601.7},0).wait(1).to({y:607.5},0).wait(1).to({y:613.4},0).wait(1).to({y:619.3},0).wait(1).to({y:625.1},0).wait(1).to({y:631},0).wait(1).to({y:636.8},0).wait(1).to({y:642.7},0).wait(1).to({y:648.5},0).wait(1).to({y:654.4},0).wait(1).to({y:660.2},0).wait(1).to({y:666.1},0).wait(1));

	// snow
	this.instance_2 = new lib.flakes("synched",0);
	this.instance_2.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({x:0.3,y:4.9},0).wait(1).to({x:0.7,y:9.9},0).wait(1).to({x:1,y:14.8},0).wait(1).to({x:1.3,y:19.8},0).wait(1).to({x:1.6,y:24.7},0).wait(1).to({x:2,y:29.6},0).wait(1).to({x:2.3,y:34.6},0).wait(1).to({x:2.6,y:39.5},0).wait(1).to({x:3,y:44.5},0).wait(1).to({x:3.3,y:49.4},0).wait(1).to({x:3.6,y:54.3},0).wait(1).to({x:4,y:59.3},0).wait(1).to({x:4.3,y:64.2},0).wait(1).to({x:4.7,y:69.1},0).wait(1).to({x:5,y:74.1},0).wait(1).to({x:5.4,y:79},0).wait(1).to({x:5.7,y:84},0).wait(1).to({x:6.1,y:88.9},0).wait(1).to({x:6.4,y:93.8},0).wait(1).to({x:6.8,y:98.8},0).wait(1).to({x:7.1,y:103.7},0).wait(1).to({x:7.5,y:108.7},0).wait(1).to({x:7.8,y:113.6},0).wait(1).to({x:8.2,y:118.5},0).wait(1).to({x:8.6,y:123.5},0).wait(1).to({x:8.9,y:128.4},0).wait(1).to({x:9.3,y:133.4},0).wait(1).to({x:9.7,y:138.3},0).wait(1).to({x:10.1,y:143.2},0).wait(1).to({x:10.5,y:148.2},0).wait(1).to({x:10.8,y:153.1},0).wait(1).to({x:11.2,y:158},0).wait(1).to({x:11.6,y:163},0).wait(1).to({x:12,y:167.9},0).wait(1).to({x:12.4,y:172.9},0).wait(1).to({x:12.8,y:177.8},0).wait(1).to({x:13.3,y:182.7},0).wait(1).to({x:13.7,y:187.7},0).wait(1).to({x:14.1,y:192.6},0).wait(1).to({x:14.5,y:197.6},0).wait(1).to({x:15,y:202.5},0).wait(1).to({x:15.4,y:207.4},0).wait(1).to({x:15.8,y:212.4},0).wait(1).to({x:16.3,y:217.3},0).wait(1).to({x:16.8,y:222.3},0).wait(1).to({x:17.2,y:227.2},0).wait(1).to({x:17.7,y:232.1},0).wait(1).to({x:18.2,y:237.1},0).wait(1).to({x:18.6,y:242},0).wait(1).to({x:19.1,y:247},0).wait(1).to({x:19.6,y:251.9},0).wait(1).to({x:20.1,y:256.8},0).wait(1).to({x:20.6,y:261.8},0).wait(1).to({x:21.1,y:266.7},0).wait(1).to({x:21.6,y:271.6},0).wait(1).to({x:22.2,y:276.6},0).wait(1).to({x:22.7,y:281.5},0).wait(1).to({x:23.2,y:286.5},0).wait(1).to({x:23.8,y:291.4},0).wait(1).to({x:24.3,y:296.3},0).wait(1).to({x:24.9,y:301.3},0).wait(1).to({x:25.5,y:306.2},0).wait(1).to({x:26.1,y:311.2},0).wait(1).to({x:26.6,y:316.1},0).wait(1).to({x:27.2,y:321},0).wait(1).to({x:27.8,y:326},0).wait(1).to({x:28.4,y:330.9},0).wait(1).to({x:29.1,y:335.9},0).wait(1).to({x:29.7,y:340.8},0).wait(1).to({x:30.3,y:345.7},0).wait(1).to({x:31,y:350.7},0).wait(1).to({x:31.6,y:355.6},0).wait(1).to({x:32.3,y:360.5},0).wait(1).to({x:33,y:365.5},0).wait(1).to({x:33.7,y:370.4},0).wait(1).to({x:34.3,y:375.4},0).wait(1).to({x:35.1,y:380.3},0).wait(1).to({x:35.8,y:385.2},0).wait(1).to({x:36.5,y:390.2},0).wait(1).to({x:37.2,y:395.1},0).wait(1).to({x:38,y:400.1},0).wait(1).to({x:38.7,y:405},0).wait(1).to({x:39.5,y:409.9},0).wait(1).to({x:40.2,y:414.9},0).wait(1).to({x:41,y:419.8},0).wait(1).to({x:41.8,y:424.8},0).wait(1).to({x:42.6,y:429.7},0).wait(1).to({x:43.5,y:434.6},0).wait(1).to({x:44.3,y:439.6},0).wait(1).to({x:45.1,y:444.5},0).wait(1).to({x:46,y:449.5},0).wait(1).to({x:46.8,y:454.4},0).wait(1).to({x:47.7,y:459.3},0).wait(1).to({x:48.6,y:464.3},0).wait(1).to({x:49.5,y:469.2},0).wait(1).to({x:50.4,y:474.1},0).wait(1).to({x:51.3,y:479.1},0).wait(1).to({x:52.3,y:484},0).wait(1).to({x:53.2,y:489},0).wait(1).to({x:54.2,y:493.9},0).wait(1).to({x:55.1,y:498.8},0).wait(1).to({x:56.1,y:503.8},0).wait(1).to({x:57.1,y:508.7},0).wait(1).to({x:58.1,y:513.7},0).wait(1).to({x:59.2,y:518.6},0).wait(1).to({x:60.2,y:523.5},0).wait(1).to({x:61.3,y:528.5},0).wait(1).to({x:62.3,y:533.4},0).wait(1).to({x:63.4,y:538.4},0).wait(1).to({x:64.5,y:543.3},0).wait(1).to({x:65.6,y:548.2},0).wait(1).to({x:66.7,y:553.2},0).wait(1).to({x:67.9,y:558.1},0).wait(1).to({x:69,y:563},0).wait(1).to({x:70.2,y:568},0).wait(1).to({x:71.4,y:572.9},0).wait(1).to({x:72.5,y:577.9},0).wait(1).to({x:73.8,y:582.8},0).wait(1).to({x:75,y:587.7},0).wait(1).to({x:76.2,y:592.7},0).wait(1).to({x:77.5,y:597.6},0).wait(1).to({x:78.7,y:602.6},0).wait(1).to({x:80,y:607.5},0).wait(1).to({x:81,y:-586.6},0).wait(1).to({x:80,y:-581.9},0).wait(1).to({x:78.7,y:-577.2},0).wait(1).to({x:77.5,y:-572.5},0).wait(1).to({x:76.2,y:-567.8},0).wait(1).to({x:75,y:-563.1},0).wait(1).to({x:73.8,y:-558.4},0).wait(1).to({x:72.6,y:-553.7},0).wait(1).to({x:71.4,y:-549.1},0).wait(1).to({x:70.2,y:-544.4},0).wait(1).to({x:69.1,y:-539.7},0).wait(1).to({x:67.9,y:-535},0).wait(1).to({x:66.8,y:-530.3},0).wait(1).to({x:65.7,y:-525.6},0).wait(1).to({x:64.6,y:-520.9},0).wait(1).to({x:63.5,y:-516.2},0).wait(1).to({x:62.4,y:-511.5},0).wait(1).to({x:61.3,y:-506.8},0).wait(1).to({x:60.3,y:-502.1},0).wait(1).to({x:59.2,y:-497.4},0).wait(1).to({x:58.2,y:-492.7},0).wait(1).to({x:57.2,y:-488},0).wait(1).to({x:56.2,y:-483.4},0).wait(1).to({x:55.2,y:-478.7},0).wait(1).to({x:54.3,y:-474},0).wait(1).to({x:53.3,y:-469.3},0).wait(1).to({x:52.4,y:-464.6},0).wait(1).to({x:51.4,y:-459.9},0).wait(1).to({x:50.5,y:-455.2},0).wait(1).to({x:49.6,y:-450.5},0).wait(1).to({x:48.7,y:-445.8},0).wait(1).to({x:47.8,y:-441.1},0).wait(1).to({x:46.9,y:-436.4},0).wait(1).to({x:46.1,y:-431.7},0).wait(1).to({x:45.2,y:-427},0).wait(1).to({x:44.4,y:-422.3},0).wait(1).to({x:43.6,y:-417.7},0).wait(1).to({x:42.8,y:-413},0).wait(1).to({x:42,y:-408.3},0).wait(1).to({x:41.2,y:-403.6},0).wait(1).to({x:40.4,y:-398.9},0).wait(1).to({x:39.6,y:-394.2},0).wait(1).to({x:38.8,y:-389.5},0).wait(1).to({x:38.1,y:-384.8},0).wait(1).to({x:37.4,y:-380.1},0).wait(1).to({x:36.6,y:-375.4},0).wait(1).to({x:35.9,y:-370.7},0).wait(1).to({x:35.2,y:-366},0).wait(1).to({x:34.5,y:-361.3},0).wait(1).to({x:33.8,y:-356.7},0).wait(1).to({x:33.1,y:-352},0).wait(1).to({x:32.4,y:-347.3},0).wait(1).to({x:31.8,y:-342.6},0).wait(1).to({x:31.1,y:-337.9},0).wait(1).to({x:30.5,y:-333.2},0).wait(1).to({x:29.9,y:-328.5},0).wait(1).to({x:29.2,y:-323.8},0).wait(1).to({x:28.6,y:-319.1},0).wait(1).to({x:28,y:-314.4},0).wait(1).to({x:27.4,y:-309.7},0).wait(1).to({x:26.8,y:-305},0).wait(1).to({x:26.2,y:-300.3},0).wait(1).to({x:25.6,y:-295.6},0).wait(1).to({x:25.1,y:-291},0).wait(1).to({x:24.5,y:-286.3},0).wait(1).to({x:24,y:-281.6},0).wait(1).to({x:23.4,y:-276.9},0).wait(1).to({x:22.9,y:-272.2},0).wait(1).to({x:22.3,y:-267.5},0).wait(1).to({x:21.8,y:-262.8},0).wait(1).to({x:21.3,y:-258.1},0).wait(1).to({x:20.8,y:-253.4},0).wait(1).to({x:20.3,y:-248.7},0).wait(1).to({x:19.8,y:-244},0).wait(1).to({x:19.3,y:-239.3},0).wait(1).to({x:18.8,y:-234.6},0).wait(1).to({x:18.3,y:-229.9},0).wait(1).to({x:17.9,y:-225.3},0).wait(1).to({x:17.4,y:-220.6},0).wait(1).to({x:16.9,y:-215.9},0).wait(1).to({x:16.5,y:-211.2},0).wait(1).to({x:16,y:-206.5},0).wait(1).to({x:15.6,y:-201.8},0).wait(1).to({x:15.2,y:-197.1},0).wait(1).to({x:14.7,y:-192.4},0).wait(1).to({x:14.3,y:-187.7},0).wait(1).to({x:13.9,y:-183},0).wait(1).to({x:13.5,y:-178.3},0).wait(1).to({x:13,y:-173.6},0).wait(1).to({x:12.6,y:-168.9},0).wait(1).to({x:12.2,y:-164.3},0).wait(1).to({x:11.8,y:-159.6},0).wait(1).to({x:11.4,y:-154.9},0).wait(1).to({x:11.1,y:-150.2},0).wait(1).to({x:10.7,y:-145.5},0).wait(1).to({x:10.3,y:-140.8},0).wait(1).to({x:9.9,y:-136.1},0).wait(1).to({x:9.5,y:-131.4},0).wait(1).to({x:9.2,y:-126.7},0).wait(1).to({x:8.8,y:-122},0).wait(1).to({x:8.4,y:-117.3},0).wait(1).to({x:8.1,y:-112.6},0).wait(1).to({x:7.7,y:-107.9},0).wait(1).to({x:7.4,y:-103.2},0).wait(1).to({x:7,y:-98.5},0).wait(1).to({x:6.6,y:-93.9},0).wait(1).to({x:6.3,y:-89.2},0).wait(1).to({x:6,y:-84.5},0).wait(1).to({x:5.6,y:-79.8},0).wait(1).to({x:5.3,y:-75.1},0).wait(1).to({x:4.9,y:-70.4},0).wait(1).to({x:4.6,y:-65.7},0).wait(1).to({x:4.3,y:-61},0).wait(1).to({x:3.9,y:-56.3},0).wait(1).to({x:3.6,y:-51.6},0).wait(1).to({x:3.3,y:-46.9},0).wait(1).to({x:2.9,y:-42.2},0).wait(1).to({x:2.6,y:-37.5},0).wait(1).to({x:2.3,y:-32.8},0).wait(1).to({x:1.9,y:-28.2},0).wait(1).to({x:1.6,y:-23.5},0).wait(1).to({x:1.3,y:-18.8},0).wait(1).to({x:1,y:-14.1},0).wait(1).to({x:0.6,y:-9.4},0).wait(1).to({x:0.3,y:-4.7},0).wait(1).to({x:0,y:0},0).wait(1));

	// snow
	this.instance_3 = new lib.flakes("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(16.3,-611.1,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({y:-606.4},0).wait(1).to({y:-601.6},0).wait(1).to({y:-596.9},0).wait(1).to({y:-592.1},0).wait(1).to({y:-587.3},0).wait(1).to({y:-582.6},0).wait(1).to({y:-577.8},0).wait(1).to({y:-573},0).wait(1).to({y:-568.3},0).wait(1).to({y:-563.5},0).wait(1).to({y:-558.7},0).wait(1).to({y:-554},0).wait(1).to({y:-549.2},0).wait(1).to({y:-544.4},0).wait(1).to({y:-539.7},0).wait(1).to({y:-534.9},0).wait(1).to({y:-530.1},0).wait(1).to({y:-525.4},0).wait(1).to({y:-520.6},0).wait(1).to({y:-515.8},0).wait(1).to({y:-511.1},0).wait(1).to({y:-506.3},0).wait(1).to({y:-501.5},0).wait(1).to({y:-496.8},0).wait(1).to({y:-492},0).wait(1).to({y:-487.2},0).wait(1).to({y:-482.5},0).wait(1).to({y:-477.7},0).wait(1).to({y:-472.9},0).wait(1).to({y:-468.2},0).wait(1).to({y:-463.4},0).wait(1).to({y:-458.6},0).wait(1).to({y:-453.9},0).wait(1).to({y:-449.1},0).wait(1).to({y:-444.4},0).wait(1).to({y:-439.6},0).wait(1).to({y:-434.8},0).wait(1).to({y:-430.1},0).wait(1).to({y:-425.3},0).wait(1).to({y:-420.5},0).wait(1).to({y:-415.8},0).wait(1).to({y:-411},0).wait(1).to({y:-406.2},0).wait(1).to({y:-401.5},0).wait(1).to({y:-396.7},0).wait(1).to({y:-391.9},0).wait(1).to({y:-387.2},0).wait(1).to({y:-382.4},0).wait(1).to({y:-377.6},0).wait(1).to({y:-372.9},0).wait(1).to({y:-368.1},0).wait(1).to({y:-363.3},0).wait(1).to({y:-358.6},0).wait(1).to({y:-353.8},0).wait(1).to({y:-349},0).wait(1).to({y:-344.3},0).wait(1).to({y:-339.5},0).wait(1).to({y:-334.7},0).wait(1).to({y:-330},0).wait(1).to({y:-325.2},0).wait(1).to({y:-320.4},0).wait(1).to({y:-315.7},0).wait(1).to({y:-310.9},0).wait(1).to({y:-306.1},0).wait(1).to({y:-301.4},0).wait(1).to({y:-296.6},0).wait(1).to({y:-291.9},0).wait(1).to({y:-287.1},0).wait(1).to({y:-282.3},0).wait(1).to({y:-277.6},0).wait(1).to({y:-272.8},0).wait(1).to({y:-268},0).wait(1).to({y:-263.3},0).wait(1).to({y:-258.5},0).wait(1).to({y:-253.7},0).wait(1).to({y:-249},0).wait(1).to({y:-244.2},0).wait(1).to({y:-239.4},0).wait(1).to({y:-234.7},0).wait(1).to({y:-229.9},0).wait(1).to({y:-225.1},0).wait(1).to({y:-220.4},0).wait(1).to({y:-215.6},0).wait(1).to({y:-210.8},0).wait(1).to({y:-206.1},0).wait(1).to({y:-201.3},0).wait(1).to({y:-196.5},0).wait(1).to({y:-191.8},0).wait(1).to({y:-187},0).wait(1).to({y:-182.2},0).wait(1).to({y:-177.5},0).wait(1).to({y:-172.7},0).wait(1).to({y:-167.9},0).wait(1).to({y:-163.2},0).wait(1).to({y:-158.4},0).wait(1).to({y:-153.6},0).wait(1).to({y:-148.9},0).wait(1).to({y:-144.1},0).wait(1).to({y:-139.3},0).wait(1).to({y:-134.6},0).wait(1).to({y:-129.8},0).wait(1).to({y:-125.1},0).wait(1).to({y:-120.3},0).wait(1).to({y:-115.5},0).wait(1).to({y:-110.8},0).wait(1).to({y:-106},0).wait(1).to({y:-101.2},0).wait(1).to({y:-96.5},0).wait(1).to({y:-91.7},0).wait(1).to({y:-86.9},0).wait(1).to({y:-82.2},0).wait(1).to({y:-77.4},0).wait(1).to({y:-72.6},0).wait(1).to({y:-67.9},0).wait(1).to({y:-63.1},0).wait(1).to({y:-58.3},0).wait(1).to({y:-53.6},0).wait(1).to({y:-48.8},0).wait(1).to({y:-44},0).wait(1).to({y:-39.3},0).wait(1).to({y:-34.5},0).wait(1).to({y:-29.7},0).wait(1).to({y:-25},0).wait(1).to({y:-20.2},0).wait(1).to({y:-15.4},0).wait(1).to({y:-10.7},0).wait(1).to({y:-5.9},0).wait(1).to({y:-1.1},0).wait(1).to({y:3.6},0).wait(1).to({y:8.4},0).wait(1).to({y:13.2},0).wait(1).to({y:17.9},0).wait(1).to({y:22.7},0).wait(1).to({y:27.4},0).wait(1).to({y:32.2},0).wait(1).to({y:37},0).wait(1).to({y:41.7},0).wait(1).to({y:46.5},0).wait(1).to({y:51.3},0).wait(1).to({y:56},0).wait(1).to({y:60.8},0).wait(1).to({y:65.6},0).wait(1).to({y:70.3},0).wait(1).to({y:75.1},0).wait(1).to({y:79.9},0).wait(1).to({y:84.6},0).wait(1).to({y:89.4},0).wait(1).to({y:94.2},0).wait(1).to({y:98.9},0).wait(1).to({y:103.7},0).wait(1).to({y:108.5},0).wait(1).to({y:113.2},0).wait(1).to({y:118},0).wait(1).to({y:122.8},0).wait(1).to({y:127.5},0).wait(1).to({y:132.3},0).wait(1).to({y:137.1},0).wait(1).to({y:141.8},0).wait(1).to({y:146.6},0).wait(1).to({y:151.4},0).wait(1).to({y:156.1},0).wait(1).to({y:160.9},0).wait(1).to({y:165.7},0).wait(1).to({y:170.4},0).wait(1).to({y:175.2},0).wait(1).to({y:180},0).wait(1).to({y:184.7},0).wait(1).to({y:189.5},0).wait(1).to({y:194.2},0).wait(1).to({y:199},0).wait(1).to({y:203.8},0).wait(1).to({y:208.5},0).wait(1).to({y:213.3},0).wait(1).to({y:218.1},0).wait(1).to({y:222.8},0).wait(1).to({y:227.6},0).wait(1).to({y:232.4},0).wait(1).to({y:237.1},0).wait(1).to({y:241.9},0).wait(1).to({y:246.7},0).wait(1).to({y:251.4},0).wait(1).to({y:256.2},0).wait(1).to({y:261},0).wait(1).to({y:265.7},0).wait(1).to({y:270.5},0).wait(1).to({y:275.3},0).wait(1).to({y:280},0).wait(1).to({y:284.8},0).wait(1).to({y:289.6},0).wait(1).to({y:294.3},0).wait(1).to({y:299.1},0).wait(1).to({y:303.9},0).wait(1).to({y:308.6},0).wait(1).to({y:313.4},0).wait(1).to({y:318.2},0).wait(1).to({y:322.9},0).wait(1).to({y:327.7},0).wait(1).to({y:332.5},0).wait(1).to({y:337.2},0).wait(1).to({y:342},0).wait(1).to({y:346.7},0).wait(1).to({y:351.5},0).wait(1).to({y:356.3},0).wait(1).to({y:361},0).wait(1).to({y:365.8},0).wait(1).to({y:370.6},0).wait(1).to({y:375.3},0).wait(1).to({y:380.1},0).wait(1).to({y:384.9},0).wait(1).to({y:389.6},0).wait(1).to({y:394.4},0).wait(1).to({y:399.2},0).wait(1).to({y:403.9},0).wait(1).to({y:408.7},0).wait(1).to({y:413.5},0).wait(1).to({y:418.2},0).wait(1).to({y:423},0).wait(1).to({y:427.8},0).wait(1).to({y:432.5},0).wait(1).to({y:437.3},0).wait(1).to({y:442.1},0).wait(1).to({y:446.8},0).wait(1).to({y:451.6},0).wait(1).to({y:456.4},0).wait(1).to({y:461.1},0).wait(1).to({y:465.9},0).wait(1).to({y:470.7},0).wait(1).to({y:475.4},0).wait(1).to({y:480.2},0).wait(1).to({y:485},0).wait(1).to({y:489.7},0).wait(1).to({y:494.5},0).wait(1).to({y:499.2},0).wait(1).to({y:504},0).wait(1).to({y:508.8},0).wait(1).to({y:513.5},0).wait(1).to({y:518.3},0).wait(1).to({y:523.1},0).wait(1).to({y:527.8},0).wait(1).to({y:532.6},0).wait(1).to({y:537.4},0).wait(1).to({y:542.1},0).wait(1).to({y:546.9},0).wait(1).to({y:551.7},0).wait(1).to({y:556.4},0).wait(1).to({y:561.2},0).wait(1).to({y:566},0).wait(1).to({y:570.7},0).wait(1).to({y:575.5},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-790,-1186.8,1596.3,1488.6);


// stage content:
(lib.top = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// headline
	this.instance = new lib.headline();
	this.instance.parent = this;
	this.instance.setTransform(-3,221);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(25).to({_off:false},0).wait(1).to({regX:241.7,regY:45.7,x:243.7,y:266.7,alpha:0.02},0).wait(1).to({x:248.5,alpha:0.041},0).wait(1).to({x:253.1,alpha:0.061},0).wait(1).to({x:257.5,alpha:0.082},0).wait(1).to({x:261.8,alpha:0.102},0).wait(1).to({x:265.8,alpha:0.122},0).wait(1).to({x:269.7,alpha:0.143},0).wait(1).to({x:273.4,alpha:0.163},0).wait(1).to({x:277,alpha:0.184},0).wait(1).to({x:280.4,alpha:0.204},0).wait(1).to({x:283.7,alpha:0.224},0).wait(1).to({x:286.8,alpha:0.245},0).wait(1).to({x:288.7,alpha:0.265},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.306},0).wait(1).to({alpha:0.327},0).wait(1).to({alpha:0.347},0).wait(1).to({alpha:0.367},0).wait(1).to({alpha:0.388},0).wait(1).to({alpha:0.408},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.469},0).wait(1).to({alpha:0.49},0).wait(1).to({alpha:0.51},0).wait(1).to({alpha:0.531},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.592},0).wait(1).to({alpha:0.612},0).wait(1).to({alpha:0.633},0).wait(1).to({alpha:0.653},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.694},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.755},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.796},0).wait(1).to({alpha:0.816},0).wait(1).to({alpha:0.837},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.878},0).wait(1).to({alpha:0.898},0).wait(1).to({alpha:0.918},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.959},0).wait(1).to({alpha:0.98},0).wait(1).to({alpha:1},0).wait(51));

	// snow
	this.instance_1 = new lib.snow();
	this.instance_1.parent = this;
	this.instance_1.setTransform(500.2,267.5,1,1.024);
	this.instance_1.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(125));

	// flame
	this.instance_2 = new lib.flame();
	this.instance_2.parent = this;
	this.instance_2.setTransform(591.9,369.6,1,1.024);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(125));

	// bg
	this.instance_3 = new lib.top_1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(0,0,1,1.024);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(125));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(222.2,-691.7,1596.3,1524.3);
// library properties:
lib.properties = {
	id: 'D9C0AEA2AE874F728A052EDE2D2A5A94',
	width: 1024,
	height: 512,
	fps: 25,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/img/top_1.jpg", id:"top_1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D9C0AEA2AE874F728A052EDE2D2A5A94'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;