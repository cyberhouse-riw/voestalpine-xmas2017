(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.signaturecontainer = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Ebene 1
	this.company = new cjs.Text("", "15px 'voestalpine Medium'");
	this.company.name = "company";
	this.company.lineHeight = 19;
	this.company.lineWidth = 931;
	this.company.parent = this;
	this.company.setTransform(2,62);

	this.department = new cjs.Text("", "15px 'voestalpine Light'");
	this.department.name = "department";
	this.department.lineHeight = 19;
	this.department.lineWidth = 931;
	this.department.parent = this;
	this.department.setTransform(2,39);

	this.signature = new cjs.Text("", "20px 'voestalpine Light'");
	this.signature.name = "signature";
	this.signature.lineHeight = 25;
	this.signature.lineWidth = 931;
	this.signature.parent = this;
	this.signature.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.signature},{t:this.department},{t:this.company}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.signaturecontainer, new cjs.Rectangle(0,0,935,89), null);


(lib.messagecontainer = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Ebene 1
	this.message = new cjs.Text("\n\n", "20px 'voestalpine Light'", "#0082B4");
	this.message.name = "message";
	this.message.lineHeight = 25;
	this.message.lineWidth = 931;
	this.message.parent = this;
	this.message.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.message).wait(1));

}).prototype = getMCSymbolPrototype(lib.messagecontainer, new cjs.Rectangle(0,0,935,233), null);


(lib.headline = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{de:1,en:2});

	// timeline functions:
	this.frame_0 = function() {
		if(window.card) {
			this.gotoAndStop(window.card.lang);
		} else {
			this.stop();
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// headline
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgJBYQgFgFAAgGQAAgJAIgEIANAAQAIAEAAAKQAAAGgFAFQgEAEgGAAQgFAAgEgFgAgEAmIgCg2IAAhMIAOAAIAABMIgEA2g");
	this.shape.setTransform(612.8,18.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgeBBIAAiAIAPAAIAAAUQANgWAZABIAIABIAAAPIgKgCQgNAAgJAIQgKAHgEAMIAABYg");
	this.shape_1.setTransform(605.7,21.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAnBcIAAhQQAAgRgKgKQgKgKgRAAQgfAAgJAfIAABWIgOAAIAAi3IAOAAIAABJQAOgVAeAAQAVAAANANQAOAOAAAWIAABSg");
	this.shape_2.setTransform(592.9,18.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgtAwQgSgUAAgcQAAgbASgUQATgTAcAAQAdAAATAUIAAgSIAOAAIAACBIgOAAIAAgRQgTATgdAAQgcAAgTgTgAgiglQgOAQAAAVQAAAWAOAQQAPAPAVAAQAhAAAPgaIAAg2QgPgaghAAQgVAAgPAQg");
	this.shape_3.setTransform(576.5,21.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgjBQQgPgOAAgXIAQAAQAAARAKAJQAKAJAOAAQAQAAAKgJQAJgJAAgRIAAiHIAQAAIAACJQAAAXgOANQgOAMgXAAQgVAAgOgNg");
	this.shape_4.setTransform(561,18.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgaA8QgMgGgFgNIAMgHQALATAUAAQANAAAIgFQAIgHAAgJQAAgJgGgHQgFgFgOgFIgMgEQgdgKAAgaQAAgOALgJQALgJAQAAQAbAAAKAYIgNAFQgHgQgSAAQgJABgGAEQgHAFAAAJQAAARAWAHIALAFQAhALAAAaQAAAOgMALQgMAKgUAAQgOAAgMgHg");
	this.shape_5.setTransform(542.9,21.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgrAwQgSgUAAgcQAAgbASgUQASgTAaAAQAbAAAQARQARARABAgIhsAAQAAAWAOAQQAOAPAWAAQAYAAAQgPIAJAKQgUATgdAAQgcAAgTgTgAAugMQgCgTgNgLQgNgLgRAAQgRAAgNAMQgNALgDASIBbAAIAAAAg");
	this.shape_6.setTransform(530.1,21.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgnA1QgNgOAAgXIAAhRIAOAAIAABPQAAASAKAKQAKAKARAAQAPAAALgJQAKgIAEgOIAAhWIAPAAIAACBIgPAAIAAgSQgOAUgeAAQgVAAgNgNg");
	this.shape_7.setTransform(514.9,21.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgrAwQgTgUAAgcQAAgbATgUQASgTAaAAQAaAAARARQASARAAAgIhsAAQAAAWAOAQQAOAPAWAAQAXAAARgPIAJAKQgUATgdAAQgcAAgTgTgAAugMQgCgTgNgLQgMgLgTAAQgPAAgNAMQgNALgEASIBbAAIAAAAg");
	this.shape_8.setTransform(500.1,21.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAnBCIAAhPQAAgTgLgJQgJgLgRAAQgfABgIAgIAABVIgQAAIAAiBIAQAAIAAASQAOgUAdAAQAVAAANANQAOAOAAAXIAABRg");
	this.shape_9.setTransform(485.1,21.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgaA8QgMgGgFgNIAMgHQALATAUAAQANAAAIgFQAIgHAAgJQAAgJgGgHQgFgFgOgFIgMgEQgdgKAAgaQAAgOALgJQALgJAQAAQAbAAAKAYIgNAFQgHgQgSAAQgJABgGAEQgHAFAAAJQAAARAWAHIALAFQAhALAAAaQAAAOgMALQgMAKgUAAQgOAAgMgHg");
	this.shape_10.setTransform(465.1,21.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgrAwQgTgUAAgcQAAgbATgUQATgTAZAAQAbAAAQARQASARAAAgIhsAAQAAAWAOAQQAPAPAUAAQAYAAARgPIAJAKQgUATgeAAQgbAAgTgTgAAugMQgCgTgNgLQgNgLgSAAQgPAAgNAMQgNALgEASIBbAAIAAAAg");
	this.shape_11.setTransform(452.3,21.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAnBcIAAhQQAAgRgKgKQgKgKgRAAQgfAAgIAfIAABWIgPAAIAAi3IAPAAIAABJQAOgVAdAAQAVAAANANQAOAOAAAWIAABSg");
	this.shape_12.setTransform(437.3,18.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgmAwQgTgUAAgcQAAgbATgUQATgTAbAAQAeAAAUATIgJALQgRgQgYgBQgVAAgOAQQgOAQAAAVQAAAWAOAQQAOAPAVAAQAYAAARgQIAJALQgUATgeAAQgbAAgTgTg");
	this.shape_13.setTransform(422.8,21.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgHBdIAAiBIAPAAIAACBgAgGhFQgGgEAAgGQAAgGAEgEQAEgDAEAAQAFAAAEADQAEAEAAAGQAAAGgFAEg");
	this.shape_14.setTransform(412.7,18.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_15.setTransform(406.7,18.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAgBcIhFhBIAABBIgOAAIAAi3IAOAAIAABxIBAg7IAUAAIhDA9IBIBEg");
	this.shape_16.setTransform(398.3,18.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgmAwQgTgUAAgcQAAgbATgUQATgTAbAAQAeAAAUATIgJALQgRgQgYgBQgVAAgOAQQgOAQAAAVQAAAWAOAQQAOAPAVAAQAYAAARgQIAJALQgUATgeAAQgbAAgTgTg");
	this.shape_17.setTransform(383.9,21.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgnBNQgNgOAAgXIAAhRIAOAAIAABPQAAASALAKQAJAKARAAQAPAAALgJQAKgIADgOIAAhWIAQAAIAACBIgQAAIAAgSQgNAUgeAAQgVAAgNgNgAAThCQgEgDAAgHQAAgFAEgEQAEgEAGAAQAFAAAEAEQAEAEAAAFQAAAHgEADQgEAEgFAAQgGAAgEgEgAgmhCQgEgDAAgHQAAgFAEgEQAFgEAFAAQAGAAAEAEQAEAEAAAFQAAAHgEADQgEAEgGAAQgFAAgFgEg");
	this.shape_18.setTransform(369.2,19.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_19.setTransform(358.7,18.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("Ag3BMIAHgNQAUAOAcAAQAVAAAOgIQAPgLAAgTQAAgNgEgHQgTATgbAAQgdAAgRgTQgSgSAAgaQAAgcASgSQARgTAdAAQAbAAATATIAAgRIAPAAIAABmQAEANAAAMQAAAbgVANQgSAMgaAAQghAAgWgPgAgjg+QgOAPAAAWQAAAVAOAOQANAPAWAAQAgAAAOgZIAAgzQgOgaggAAQgWAAgNAPg");
	this.shape_20.setTransform(347.3,23.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AAnBCIAAhPQAAgTgLgJQgJgLgRAAQgfABgIAgIAABVIgPAAIAAiBIAPAAIAAASQAOgUAdAAQAVAAANANQAOAOAAAXIAABRg");
	this.shape_21.setTransform(325.2,21.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgGBdIAAiBIANAAIAACBgAgGhFQgGgEAAgGQAAgGAEgEQAEgDAEAAQAFAAAEADQAEAEAAAGQAAAGgGAEg");
	this.shape_22.setTransform(314.6,18.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgrAwQgSgUAAgcQAAgbASgUQASgTAaAAQAbAAAQARQARARABAgIhsAAQAAAWAOAQQAOAPAWAAQAYAAAQgPIAJAKQgUATgdAAQgcAAgTgTgAAugMQgCgTgNgLQgMgLgSAAQgRAAgNAMQgNALgDASIBbAAIAAAAg");
	this.shape_23.setTransform(304.2,21.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgtBKQgSgUAAgcQAAgbASgTQATgUAcAAQAdAAATAUIAAhIIAOAAIAAC3IgOAAIAAgRQgTATgdAAQgcAAgTgTgAgigKQgOAOAAAWQAAAWAOAQQAPAQAVAAQAhAAAPgaIAAg2QgPgaghAAQgVAAgPAQg");
	this.shape_24.setTransform(281.2,18.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AAnBCIAAhPQAAgTgKgJQgKgLgRAAQgfABgJAgIAABVIgOAAIAAiBIAOAAIAAASQAOgUAeAAQAVAAANANQANAOAAAXIAABRg");
	this.shape_25.setTransform(266,21.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgnA1QgOgOAAgXIAAhRIAPAAIAABPQAAASALAKQAJAKARAAQAPAAALgJQAKgIADgOIAAhWIAPAAIAACBIgPAAIAAgSQgOAUgdAAQgVAAgNgNg");
	this.shape_26.setTransform(250.7,21.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AAnBCIAAhPQAAgTgKgJQgKgLgRAAQgfABgJAgIAABVIgOAAIAAiBIAOAAIAAASQAOgUAeAAQAVAAANANQANAOAAAXIAABRg");
	this.shape_27.setTransform(229.1,21.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgrAwQgSgUgBgcQABgbASgUQASgTAaAAQAbAAAQARQARARABAgIhsAAQAAAWAOAQQAOAPAWAAQAYAAAQgPIAJAKQgUATgdAAQgcAAgTgTgAAugMQgCgTgNgLQgMgLgTAAQgQAAgNAMQgNALgDASIBbAAIAAAAg");
	this.shape_28.setTransform(214.1,21.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AAWBUQgjAAAAgiIAAhQIgXAAIgDgOIAaAAIAAggIAOgIIAAAoIAnAAIgEAOIgjAAIAABPQAAAMAEAFQAFAEAMAAIAQAAIAAAOg");
	this.shape_29.setTransform(201.9,19.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AAnBcIAAhQQAAgRgKgKQgKgKgRAAQgfAAgJAfIAABWIgOAAIAAi3IAOAAIAABJQAPgVAdAAQAVAAANANQAOAOAAAWIAABSg");
	this.shape_30.setTransform(190,18.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgmAwQgTgUAAgcQAAgbATgUQATgTAbAAQAeAAAUATIgJALQgRgQgYgBQgVAAgOAQQgOAQAAAVQAAAWAOAQQAOAPAVAAQAYAAARgQIAJALQgUATgeAAQgbAAgTgTg");
	this.shape_31.setTransform(175.6,21.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgtAwQgSgUAAgcQAAgbASgUQATgTAcAAQAdAAATAUIAAgSIAOAAIAACBIgOAAIAAgRQgTATgdAAQgcAAgTgTgAgiglQgOAQAAAVQAAAWAOAQQAPAPAVAAQAhAAAPgaIAAg2QgPgaghAAQgVAAgPAQg");
	this.shape_32.setTransform(159.7,21.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AAnBCIAAhPQAAgTgLgJQgJgLgRAAQgfABgIAgIAABVIgQAAIAAiBIAQAAIAAASQAOgUAdAAQAVAAANANQANAOAAAXIAABRg");
	this.shape_33.setTransform(144.5,21.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AAnBcIAAhQQAAgRgLgKQgJgKgRAAQgfAAgIAfIAABWIgQAAIAAi3IAQAAIAABJQANgVAeAAQAVAAANANQANAOAAAWIAABSg");
	this.shape_34.setTransform(129.4,18.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgHBdIAAiBIAPAAIAACBgAgGhFQgGgEAAgGQAAgGAEgEQAEgDAEAAQAGAAADADQAEAEAAAGQAAAGgFAEg");
	this.shape_35.setTransform(118.8,18.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgrAwQgTgUABgcQgBgbATgUQATgTAZAAQAbAAAQARQASARABAgIhtAAQAAAWAOAQQAPAPAUAAQAYAAARgPIAKAKQgVATgeAAQgbAAgTgTgAAugMQgCgTgNgLQgNgLgSAAQgPAAgNAMQgNALgEASIBbAAIAAAAg");
	this.shape_36.setTransform(108.4,21.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AAAg0IgyCSIg+i5IAQAAIAvCNIAxiPIAyCPIAviNIAQAAIg+C5g");
	this.shape_37.setTransform(89,18.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AgrAwQgSgUAAgcQAAgbASgUQATgTAZAAQAaAAARARQARARACAgIhtAAQAAAWAOAQQAOAPAVAAQAZAAAQgPIAKAKQgVATgeAAQgbAAgTgTgAAugMQgCgTgNgLQgNgLgRAAQgQAAgOAMQgNALgDASIBbAAIAAAAg");
	this.shape_38.setTransform(63.2,21.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AAnBcIAAhQQAAgRgKgKQgKgKgRAAQgfAAgJAfIAABWIgOAAIAAi3IAOAAIAABJQAOgVAeAAQAVAAANANQANAOAAAWIAABSg");
	this.shape_39.setTransform(48.2,18.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQATgTAcAAQAdAAAUATQASAUAAAbQAAAcgSAUQgUATgdAAQgcAAgTgTgAglglQgOAPAAAWQAAAWAOAQQAPAPAWAAQAXAAAPgPQAOgQAAgWQAAgWgOgPQgPgQgXAAQgWAAgPAQg");
	this.shape_40.setTransform(32.4,21.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AgeBBIAAiAIAPAAIAAAUQANgWAZABIAIABIAAAPIgKgCQgNAAgJAIQgKAHgEAMIAABYg");
	this.shape_41.setTransform(20.7,21.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AgxBcIAAi3IBjAAIAAAPIhTAAIAABCIBAAAIAAAPIhAAAIAABXg");
	this.shape_42.setTransform(9.7,18.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgKBYQgEgFAAgGQAAgJAIgEIANAAQAIAEAAAKQAAAGgEAFQgFAEgGAAQgFAAgFgFgAgEAmIgCg2IAAhMIANAAIAABMIgDA2g");
	this.shape_43.setTransform(491.9,18.9);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgHBcIAAhOIhDhpIASAAIA4BbIA5hbIASAAIhDBpIAABOg");
	this.shape_44.setTransform(440,18.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AAiBCIgihdIggBdIgLAAIgqiBIAQAAIAgBnIAlhpIAmBpIAhhnIAPAAIgpCBg");
	this.shape_45.setTransform(416.1,21.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AgrAwQgSgUAAgcQAAgbASgUQATgTAZAAQAbAAAQARQARARACAgIhtAAQAAAWAOAQQAPAPAUAAQAZAAAQgPIAKAKQgVATgeAAQgbAAgTgTgAAugMQgCgTgNgLQgMgLgSAAQgRAAgNAMQgNALgDASIBbAAIAAAAg");
	this.shape_46.setTransform(399.7,21.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_47.setTransform(381.6,18.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgkBZIAdg/Ig4hyIARAAIAuBkIAvhkIAQAAIhUCxg");
	this.shape_48.setTransform(357.8,23.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("Ag/BaIAAixIAOAAIAAASQATgUAeAAQAbAAATATQASAUAAAcQAAAbgSAUQgTATgbAAQgeAAgTgUIAABCgAgehEQgMAIgHALIAAA1QAPAaAiAAQAUAAAPgPQAOgPAAgWQAAgWgOgQQgPgQgUAAQgRABgNAHg");
	this.shape_49.setTransform(343.3,23.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("Ag/BaIAAixIAPAAIAAASQASgUAeAAQAbAAATATQASAUAAAcQAAAbgSAUQgTATgbAAQgeAAgSgUIAABCgAgehEQgMAIgGALIAAA1QAOAaAiAAQAVAAAOgPQAOgPAAgWQAAgWgOgQQgOgQgVAAQgRABgNAHg");
	this.shape_50.setTransform(326.8,23.8);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AA5BcIAAhXIhxAAIAABXIgQAAIAAi3IAQAAIAABRIBxAAIAAhRIAQAAIAAC3g");
	this.shape_51.setTransform(291.5,18.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AgtAwQgSgUAAgcQAAgbASgUQATgTAcAAQAdAAATAUIAAgSIAOAAIAACBIgOAAIAAgRQgTATgdAAQgcAAgTgTgAgiglQgOAQAAAVQAAAWAOAQQAPAPAVAAQAhAAAPgaIAAg2QgPgaghAAQgVAAgPAQg");
	this.shape_52.setTransform(265.9,21.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AAnBCIAAhPQAAgTgLgJQgJgLgRAAQgfABgJAgIAABVIgPAAIAAiBIAPAAIAAASQAOgUAeAAQAVAAANANQANAOAAAXIAABRg");
	this.shape_53.setTransform(227.5,21.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#000000").s().p("AgtAwQgSgUAAgcQAAgbASgUQATgTAcAAQAdAAATAUIAAgSIAOAAIAACBIgOAAIAAgRQgTATgdAAQgcAAgTgTgAgiglQgOAQAAAVQAAAWAOAQQAPAPAVAAQAhAAAPgaIAAg2QgPgaghAAQgVAAgPAQg");
	this.shape_54.setTransform(211.1,21.5);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#000000").s().p("AgtAwQgSgUAAgcQAAgbASgUQATgTAcAAQAdAAATAUIAAgSIAOAAIAACBIgOAAIAAgRQgTATgdAAQgcAAgTgTgAgiglQgOAQAAAVQAAAWAOAQQAPAPAVAAQAhAAAPgaIAAg2QgPgaghAAQgVAAgPAQg");
	this.shape_55.setTransform(176.8,21.5);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#000000").s().p("ABOBCIAAhPQAAgSgKgKQgJgLgPAAQgcAAgJAgIAAAFIAABRIgNAAIAAhPQAAgSgKgKQgJgLgPAAQgbAAgJAdIAABZIgPAAIAAiBIAPAAIAAARQAOgTAaAAQAbAAAKAYQAQgYAdAAQAUAAANANQANAOAAAXIAABRg");
	this.shape_56.setTransform(157.8,21.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AgHBdIAAiBIAOAAIAACBgAgGhFQgGgEAAgGQAAgGAEgEQAEgDAEAAQAGAAADADQAEAEAAAGQAAAGgGAEg");
	this.shape_57.setTransform(123.1,18.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AAnBcIAAhQQAAgRgLgKQgJgKgRAAQgfAAgIAfIAABWIgQAAIAAi3IAQAAIAABJQAOgVAdAAQAVAAANANQANAOABAWIAABSg");
	this.shape_58.setTransform(103.6,18.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AgwBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAiAAAZAUIgJANQgVgSgdAAQggAAgWAXQgXAWAAAhQAAAiAXAWQAWAXAgAAQAdAAAVgSIAJANQgZAUgiAAQgnAAgbgbg");
	this.shape_59.setTransform(87.4,18.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AglBZIAfg/Ig4hyIAQAAIAuBkIAvhkIARAAIhVCxg");
	this.shape_60.setTransform(64.9,23.9);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AgeBBIAAiAIAPAAIAAAUQANgWAZABIAIABIAAAPIgKgCQgNAAgJAIQgKAHgEAMIAABYg");
	this.shape_61.setTransform(54.6,21.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AgeBBIAAiAIAPAAIAAAUQANgWAZABIAIABIAAAPIgKgCQgNAAgJAIQgKAHgEAMIAABYg");
	this.shape_62.setTransform(45.5,21.5);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#000000").s().p("AgrAwQgTgUAAgcQAAgbATgUQATgTAZAAQAbAAAQARQASARABAgIhtAAQAAAWAOAQQAPAPAUAAQAZAAAQgPIAKAKQgVATgeAAQgbAAgTgTgAAugMQgCgTgNgLQgNgLgSAAQgPAAgNAMQgNALgEASIBbAAIAAAAg");
	this.shape_63.setTransform(32.8,21.5);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("ABJBdIAAiQIhJBSIhIhSIAACQIgQAAIAAi5IBYBkIBZhkIAAC5g");
	this.shape_64.setTransform(13.6,18.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_42},{t:this.shape_41,p:{x:20.7}},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38,p:{x:63.2}},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32,p:{x:159.7}},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29,p:{x:201.9}},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24,p:{x:281.2}},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10,p:{x:465.1}},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5,p:{x:542.9}},{t:this.shape_4},{t:this.shape_3,p:{x:576.5}},{t:this.shape_2},{t:this.shape_1,p:{x:605.7}},{t:this.shape}]},1).to({state:[{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_41,p:{x:116.4}},{t:this.shape_57},{t:this.shape_10,p:{x:131.6}},{t:this.shape_29,p:{x:141.6}},{t:this.shape_56},{t:this.shape_55},{t:this.shape_5,p:{x:191.1}},{t:this.shape_54},{t:this.shape_53},{t:this.shape_24,p:{x:242.7}},{t:this.shape_52},{t:this.shape_51},{t:this.shape_32,p:{x:309.3}},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_38,p:{x:455.6}},{t:this.shape_3,p:{x:470.7}},{t:this.shape_1,p:{x:484.8}},{t:this.shape_43}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


// stage content:
(lib.preview = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var timeout;                                                         
		
		var update = (function () {
			timeout = null;
			if(window.card) {
				this.headline.gotoAndStop(window.card.lang);
				this.messagecontainer.message.text = window.card.message;
				this.signaturecontainer.signature.text = window.card.signature;
				this.signaturecontainer.company.text = window.card.company;
				this.signaturecontainer.department.text = window.card.department;
			} else {
				this.headline.gotoAndStop(1);
			}
		}).bind(this);
		
		window.updateCard = function() {
			if(timeout) {
				clearTimeout(timeout);
			}
			timeout = setTimeout(update, 50);
		};
		
		update();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// headline
	this.headline = new lib.headline();
	this.headline.name = "headline";
	this.headline.parent = this;
	this.headline.setTransform(45,43.2,1,1.024,0,0,0,0,0.5);

	this.timeline.addTween(cjs.Tween.get(this.headline).wait(1));

	// message
	this.messagecontainer = new lib.messagecontainer();
	this.messagecontainer.name = "messagecontainer";
	this.messagecontainer.parent = this;
	this.messagecontainer.setTransform(45,136.2,1,1.024);

	this.timeline.addTween(cjs.Tween.get(this.messagecontainer).wait(1));

	// signature
	this.signaturecontainer = new lib.signaturecontainer();
	this.signaturecontainer.name = "signaturecontainer";
	this.signaturecontainer.parent = this;
	this.signaturecontainer.setTransform(45,394.2,1,1.024,0,0,0,0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.signaturecontainer).wait(1));

	// bg
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhP/AoAMAAAhP/MCf/AAAMAAABP/g");
	this.shape.setTransform(512,256);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(512,256,1024,512);
// library properties:
lib.properties = {
	id: '933BC9B3B3754B3D977DD753940E6191',
	width: 1024,
	height: 512,
	fps: 25,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['933BC9B3B3754B3D977DD753940E6191'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;