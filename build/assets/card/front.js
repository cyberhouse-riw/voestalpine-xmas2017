(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.front_1 = function() {
	this.initialize(img.front_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,500);


(lib.light = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Ebene 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0.8)","rgba(237,210,132,0)"],[0,1],0,0,0,0,0,31).s().p("AjZDaQhZhbgBh/QABh+BZhbQBbhZB+gBQB/ABBbBZQBZBbABB+QgBB/hZBbQhbBZh/ABQh+gBhbhZg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30.7,-30.7,61.5,61.5);


(lib.headline = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{de:1,en:2});

	// timeline functions:
	this.frame_0 = function() {
		this.gotoAndStop(window.card.lang);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// headline
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKALQgEgEAAgGQAAgHAEgEQAFgFAFABQAHgBAEAFQAEAEAAAHQAAAGgEAEQgEAFgHgBQgFABgFgFg");
	this.shape.setTransform(391.5,80.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKALQgEgEAAgGQAAgHAEgEQAFgFAFABQAHgBAEAFQAEAEAAAHQAAAGgEAEQgEAFgHgBQgFABgFgFg");
	this.shape_1.setTransform(385.2,80.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKALQgEgEAAgGQAAgHAEgEQAFgFAFABQAHgBAEAFQAEAEAAAHQAAAGgEAEQgEAFgHgBQgFABgFgFg");
	this.shape_2.setTransform(378.8,80.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgIBcIAAioIg0AAIAAgPIB5AAIAAAPIg2AAIAACog");
	this.shape_3.setTransform(362.4,72.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgzBDQgbgbAAgoQAAgnAbgbQAcgbAnAAQAiAAAbATIgJANQgXgRgdAAQghAAgWAXQgXAWAAAhQAAAiAXAWQAWAXAhAAQAaAAAVgLIAAg/Ig2AAIAHgPIA/AAIAABWQgcASgjAAQgnAAgcgbg");
	this.shape_4.setTransform(346.1,72.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_5.setTransform(326.3,72.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_6.setTransform(312.2,72.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAsBcIhEhTIgXAAIAABTIgQAAIAAi3IAwAAQAcAAAPANQAQANABAYQgBAWgNANQgNAMgXADIBFBTgAgvgFIAgAAQArAAgBgkQABgjgrAAIggAAg");
	this.shape_7.setTransform(301.9,72.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ag9BcIAAi3IA5AAQAZAAAOAOQAOANABATQgBAcgWAMQAPAFAJAKQALANAAATQAAAYgTAOQgSAMgeAAgAgtBNIAtAAQAWAAAMgKQALgJAAgRQAAgRgLgKQgMgKgWAAIgtAAgAgtgKIArAAQAQAAAKgJQAJgJAAgPQAAgOgJgKQgKgJgQAAIgrAAg");
	this.shape_8.setTransform(285.1,72.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAsBcIhEhTIgYAAIAABTIgPAAIAAi3IAwAAQAcAAAQANQAPANABAYQgBAWgNANQgNAMgXADIBGBTgAgwgFIAhAAQArAAAAgkQAAgjgrAAIghAAg");
	this.shape_9.setTransform(269.4,72.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_10.setTransform(253.6,72.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgHBcIAAioIg0AAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_11.setTransform(239.1,72.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_12.setTransform(228.9,72.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_13.setTransform(218.7,72.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAAg0IgyCSIg+i5IAQAAIAvCNIAxiPIAyCPIAviNIAQAAIg+C5g");
	this.shape_14.setTransform(198.9,72.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_15.setTransform(173.8,72.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgyBcIAAi3IAQAAIAACoIBVAAIAAAPg");
	this.shape_16.setTransform(160.3,72.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgyBcIAAi3IARAAIAACoIBTAAIAAAPg");
	this.shape_17.setTransform(146.8,72.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_18.setTransform(130,72.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQATAAALgJQAMgIAAgPQgBgOgIgKQgIgIgRgHIgTgIQgngQAAgiQAAgTAQgNQAPgLAUAAQAgAAAUAXIgMALQgQgTgZAAQgOAAgKAIQgKAIAAANQAAAXAcAMIATAIQAtASAAAhQAAAWgPANQgQAOgbAAQgRAAgRgIg");
	this.shape_19.setTransform(107.3,72.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_20.setTransform(89.9,72.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgzBLQgVgTABghIAAhzIAQAAIAAB0QAAAaAPAOQAPAOAZAAQAaAAAPgOQAPgOAAgaIAAh0IARAAIAABzQAAAhgVATQgTASghAAQgfAAgUgSg");
	this.shape_21.setTransform(69.6,72.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIANgMQARAWAbAAQATAAALgJQALgIAAgPQABgOgKgKQgHgIgSgHIgSgIQgngQAAgiQAAgTARgNQAOgLAVAAQAfAAAUAXIgMALQgQgTgYAAQgOAAgKAIQgLAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgQANQgPAOgbAAQgSAAgQgIg");
	this.shape_22.setTransform(46,72.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_23.setTransform(30.2,72.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AhLBcIAAi3IAzAAQArAAAdAaQAcAaAAAnQAAAogcAaQgdAagrAAgAg7BNIAjAAQAlAAAXgWQAXgVAAgiQAAghgXgVQgXgWglAAIgjAAg");
	this.shape_24.setTransform(12.2,72.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgMAgIAMgjIgLAAIAAgbIAYAAIAAAbIgNAjg");
	this.shape_25.setTransform(370.8,55.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_26.setTransform(357,45.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_27.setTransform(340,45.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgHBcIAAioIg0AAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_28.setTransform(325.5,45.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_29.setTransform(315.3,45.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_30.setTransform(305.1,45.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAtBcIhghYIAABYIgQAAIAAi3IAQAAIAABbIBbhbIAVAAIhcBdIBjBag");
	this.shape_31.setTransform(290.3,45.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgzBDQgbgbAAgoQAAgnAbgbQAcgbAnAAQAiAAAbATIgJANQgXgRgdAAQghAAgWAXQgWAWgBAhQABAiAWAWQAWAXAhAAQAaAAAVgLIAAg/Ig2AAIAHgPIA/AAIAABWQgbASgkAAQgnAAgcgbg");
	this.shape_32.setTransform(271.1,45.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_33.setTransform(258.1,45.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AA5BcIAAhXIhxAAIAABXIgQAAIAAi3IAQAAIAABRIBxAAIAAhRIAQAAIAAC3g");
	this.shape_34.setTransform(244.5,45.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("ABIB0Igbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAtIgmhPIglBPIBLAAgAAThcQgEgEAAgGQAAgGAEgEQAEgDAGAAQAFAAAEADQAEAEAAAGQAAAGgEAEQgEAEgFAAQgGAAgEgEgAglhcQgEgEAAgGQAAgGAEgEQAEgDAFAAQAGAAAEADQAEAEAAAGQAAAGgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_35.setTransform(225.4,43.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgxBcIAAi3IBjAAIAAAPIhTAAIAABCIBAAAIAAAPIhAAAIAABXg");
	this.shape_36.setTransform(210.8,45.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_37.setTransform(185.8,45.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_38.setTransform(168.8,45.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_39.setTransform(150.3,45.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_40.setTransform(133.3,45.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgzBDQgbgbAAgoQAAgnAbgbQAcgbAnAAQAjAAAZATIgJANQgVgRgeAAQghAAgWAXQgXAWAAAhQAAAiAXAWQAWAXAhAAQAbAAAUgLIAAg/Ig2AAIAHgPIA/AAIAABWQgcASgjAAQgnAAgcgbg");
	this.shape_41.setTransform(115.6,45.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_42.setTransform(102.7,45.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_43.setTransform(92.5,45.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_44.setTransform(71.2,45.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_45.setTransform(59.7,45.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AhLBcIAAi3IAzAAQArAAAdAaQAcAaAAAnQAAAogcAaQgdAagrAAgAg7BNIAjAAQAlAAAXgWQAXgVAAgiQAAghgXgVQgXgWglAAIgjAAg");
	this.shape_46.setTransform(47.1,45.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_47.setTransform(19.5,45.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_48.setTransform(5.5,45.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("Ag8g2IAACSIgQAAIAAi5ICJCUIAAiSIAQAAIAAC5g");
	this.shape_49.setTransform(275.5,18.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_50.setTransform(258.5,18.8);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgzBLQgVgTABghIAAhzIAQAAIAAB0QAAAaAPAOQAPAOAZAAQAaAAAPgOQAPgOAAgaIAAh0IAQAAIAABzQABAhgVATQgTASghAAQggAAgTgSg");
	this.shape_51.setTransform(240.7,18.9);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_52.setTransform(222,18.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AAsBcIhEhTIgYAAIAABTIgPAAIAAi3IAwAAQAcAAAQANQAQANAAAYQAAAWgOANQgNAMgXADIBGBTgAgwgFIAhAAQArAAAAgkQAAgjgrAAIghAAg");
	this.shape_53.setTransform(206.3,18.8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgIBcIAAioIg0AAIAAgPIB5AAIAAAPIg1AAIAACog");
	this.shape_54.setTransform(190.5,18.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AAsBcIhDhTIgZAAIAABTIgQAAIAAi3IAyAAQAbAAAQANQAPANAAAYQAAAWgMANQgOAMgWADIBFBTgAgwgFIAhAAQAqAAAAgkQAAgjgqAAIghAAg");
	this.shape_55.setTransform(177.2,18.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_56.setTransform(161.4,18.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AhShcIAQAAIBCCVIBDiVIAQAAIhTC5g");
	this.shape_57.setTransform(144.8,18.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQATAAALgJQALgIAAgPQABgOgJgKQgIgIgSgHIgSgIQgngQAAgiQAAgTARgNQAPgLAUAAQAfAAAUAXIgMALQgQgTgZAAQgOAAgJAIQgLAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgQANQgQAOgaAAQgSAAgQgIg");
	this.shape_58.setTransform(122.3,18.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_59.setTransform(106.5,18.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhLBcIAAi3IAzAAQArAAAdAaQAcAaAAAnQAAAogcAaQgdAagrAAgAg7BNIAjAAQAlAAAXgWQAXgVAAgiQAAghgXgVQgXgWglAAIgjAAg");
	this.shape_60.setTransform(88.5,18.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgHBcIAAioIg0AAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_61.setTransform(64.9,18.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQATAAALgJQALgIAAgPQABgOgJgKQgIgIgSgHIgSgIQgngQAAgiQAAgTARgNQAPgLAUAAQAfAAAUAXIgMALQgQgTgZAAQgOAAgJAIQgLAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgQANQgQAOgaAAQgSAAgQgIg");
	this.shape_62.setTransform(51.2,18.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgHBcIAAi3IAPAAIAAC3g");
	this.shape_63.setTransform(40.7,18.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQATAAALgJQALgIAAgPQABgOgKgKQgHgIgSgHIgSgIQgngQAAgiQAAgTARgNQAPgLAUAAQAfAAAUAXIgMALQgQgTgZAAQgOAAgJAIQgLAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgQANQgQAOgaAAQgSAAgQgIg");
	this.shape_64.setTransform(23.3,18.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgzBcIAAi3IBnAAIAAAPIhXAAIAABCIBHAAIAAAPIhHAAIAABIIBXAAIAAAPg");
	this.shape_65.setTransform(9.8,18.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#FFFFFF").ss(1.5,1,1).p("A+dAAMA87AAA");
	this.shape_66.setTransform(198.3,91);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AAsBcIhEhTIgXAAIAABTIgQAAIAAi3IAwAAQAcAAAPANQAQANABAYQgBAWgNANQgNAMgXADIBFBTgAgvgFIAgAAQAqAAAAgkQAAgjgqAAIggAAg");
	this.shape_67.setTransform(320.8,72.7);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AAsBcIhDhTIgZAAIAABTIgQAAIAAi3IAyAAQAbAAAQANQAQANAAAYQAAAWgOANQgNAMgXADIBGBTgAgwgFIAhAAQArAAAAgkQAAgjgrAAIghAAg");
	this.shape_68.setTransform(263.4,72.7);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AhCBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAoAAAbAbQAbAbAAAnQAAAogbAbQgbAbgoAAQgnAAgbgbgAg2g3QgXAWAAAhQAAAiAXAWQAWAXAgAAQAhAAAXgXQAWgWAAgiQAAghgWgWQgXgXghAAQggAAgWAXg");
	this.shape_69.setTransform(243.2,72.7);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgxBcIAAi3IBjAAIAAAPIhTAAIAABCIBBAAIAAAPIhBAAIAABXg");
	this.shape_70.setTransform(226.7,72.7);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AhThcIARAAIBCCVIBDiVIARAAIhUC5g");
	this.shape_71.setTransform(189,72.8);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AhCBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAoAAAbAbQAbAbAAAnQAAAogbAbQgbAbgoAAQgnAAgbgbgAg2g3QgXAWAAAhQAAAiAXAWQAWAXAgAAQAhAAAXgXQAWgWAAgiQAAghgWgWQgXgXghAAQggAAgWAXg");
	this.shape_72.setTransform(169.5,72.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("ABJBdIAAiQIhJBSIhIhSIAACQIgQAAIAAi5IBYBkIBZhkIAAC5g");
	this.shape_73.setTransform(147,72.6);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AhCBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAoAAAbAbQAbAbAAAnQAAAogbAbQgbAbgoAAQgnAAgbgbgAg2g3QgXAWAAAhQAAAiAXAWQAWAXAgAAQAhAAAXgXQAWgWAAgiQAAghgWgWQgXgXghAAQggAAgWAXg");
	this.shape_74.setTransform(117.7,72.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_75.setTransform(100.3,72.7);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AgxBcIAAi3IAQAAIAACoIBUAAIAAAPg");
	this.shape_76.setTransform(81.4,72.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQASAAAMgJQALgIAAgPQABgOgKgKQgHgIgSgHIgSgIQgngQAAgiQAAgTARgNQAOgLAVAAQAfAAAUAXIgMALQgQgTgYAAQgPAAgJAIQgLAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgQANQgQAOgaAAQgSAAgQgIg");
	this.shape_77.setTransform(28.4,72.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQASAAAMgJQALgIAAgPQABgOgKgKQgHgIgSgHIgSgIQgngQAAgiQAAgTARgNQAOgLAVAAQAfAAAUAXIgMALQgQgTgYAAQgPAAgJAIQgLAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgQANQgQAOgaAAQgSAAgQgIg");
	this.shape_78.setTransform(295.7,45.7);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AgyBcIAAi3IAQAAIAACoIBVAAIAAAPg");
	this.shape_79.setTransform(268.6,45.7);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("Ag9BcIAAi3IA4AAQAaAAAPAOQANANAAATQAAAcgWAMQAPAFAKAKQAKANAAATQAAAYgTAOQgSAMgfAAgAgtBNIAuAAQAWAAAMgKQAKgJABgRQgBgRgKgKQgMgKgWAAIguAAgAgtgKIArAAQAQAAAKgJQAKgJAAgPQAAgOgKgKQgLgJgPAAIgrAAg");
	this.shape_80.setTransform(253.2,45.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_81.setTransform(177.6,45.7);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_82.setTransform(126.4,45.7);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIANgMQARAWAbAAQASAAAMgJQALgIAAgPQAAgOgJgKQgHgIgRgHIgTgIQgngQAAgiQAAgTARgNQAOgLAVAAQAfAAAUAXIgMALQgQgTgYAAQgOAAgLAIQgKAIAAANQAAAXAcAMIATAIQAtASAAAhQAAAWgQANQgPAOgbAAQgRAAgRgIg");
	this.shape_83.setTransform(106,45.7);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AgyBcIAAi3IAQAAIAACoIBVAAIAAAPg");
	this.shape_84.setTransform(51.4,45.7);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("Ag9BcIAAi3IA4AAQAaAAAPAOQANANAAATQAAAcgWAMQAQAFAJAKQAKANAAATQAAAYgTAOQgSAMgfAAgAgtBNIAtAAQAXAAAMgKQALgJgBgRQABgRgLgKQgMgKgXAAIgtAAgAgtgKIArAAQAQAAAKgJQAKgJAAgPQAAgOgKgKQgLgJgPAAIgrAAg");
	this.shape_85.setTransform(28.8,45.7);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_86.setTransform(10.8,45.6);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AAAg0IgyCSIg+i5IAQAAIAvCNIAxiPIAyCPIAviNIAQAAIg+C5g");
	this.shape_87.setTransform(267.8,18.8);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("AhCBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAoAAAbAbQAbAbAAAnQAAAogbAbQgbAbgoAAQgnAAgbgbgAg2g3QgXAWAAAhQAAAiAXAWQAWAXAgAAQAhAAAXgXQAWgWAAgiQAAghgWgWQgXgXghAAQggAAgWAXg");
	this.shape_88.setTransform(245.1,18.8);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AAsBcIhEhTIgYAAIAABTIgPAAIAAi3IAwAAQAcAAAPANQAQANABAYQgBAWgNANQgNAMgXADIBFBTgAgwgFIAhAAQArAAAAgkQAAgjgrAAIghAAg");
	this.shape_89.setTransform(220.8,18.8);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AgzBLQgVgTAAghIAAhzIARAAIAAB0QgBAaARAOQAOAOAZAAQAaAAAPgOQAPgOAAgaIAAh0IAQAAIAABzQABAhgVATQgUASggAAQggAAgTgSg");
	this.shape_90.setTransform(201.8,18.9);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AhCBDQgbgbAAgoQAAgnAbgbQAbgbAnAAQAoAAAbAbQAbAbAAAnQAAAogbAbQgbAbgoAAQgnAAgbgbgAg2g3QgXAWAAAhQAAAiAXAWQAWAXAgAAQAhAAAXgXQAWgWAAgiQAAghgWgWQgXgXghAAQggAAgWAXg");
	this.shape_91.setTransform(181.1,18.8);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AA5BcIAAhXIhxAAIAABXIgQAAIAAi3IAQAAIAABRIBxAAIAAhRIAQAAIAAC3g");
	this.shape_92.setTransform(118.6,18.8);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_93.setTransform(102,18.8);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("ABIBdIgbg4IhZAAIgbA4IgRAAIBYi5IBZC5gAAmAWIgmhPIglBPIBLAAg");
	this.shape_94.setTransform(79.2,18.7);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("AgxBcIAAi3IBjAAIAAAPIhTAAIAABCIBBAAIAAAPIhBAAIAABXg");
	this.shape_95.setTransform(64.6,18.8);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("AgiBWQgOgHgJgKIAMgMQASAWAbAAQATAAALgJQAMgIAAgPQAAgOgJgKQgIgIgSgHIgSgIQgngQAAgiQAAgTAQgNQAQgLATAAQAgAAAUAXIgMALQgQgTgZAAQgOAAgJAIQgLAIAAANQAAAXAdAMIASAIQAtASAAAhQAAAWgPANQgQAOgbAAQgRAAgRgIg");
	this.shape_96.setTransform(43,18.8);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AgIBcIAAioIgzAAIAAgPIB3AAIAAAPIg1AAIAACog");
	this.shape_97.setTransform(15.7,18.8);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#FFFFFF").ss(1.5,1,1).p("A8vAAMA5fAAA");
	this.shape_98.setTransform(187.3,91);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59,p:{x:106.5,y:18.7}},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52,p:{x:222,y:18.7}},{t:this.shape_51,p:{x:240.7,y:18.9}},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48,p:{y:45.7}},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45,p:{x:59.7,y:45.7}},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42,p:{x:102.7,y:45.7}},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34,p:{x:244.5}},{t:this.shape_33,p:{x:258.1,y:45.7}},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29,p:{x:315.3}},{t:this.shape_28},{t:this.shape_27,p:{x:340}},{t:this.shape_26,p:{x:357,y:45.7}},{t:this.shape_25},{t:this.shape_24,p:{x:12.2}},{t:this.shape_23,p:{x:30.2}},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20,p:{x:89.9,y:72.7}},{t:this.shape_19},{t:this.shape_18,p:{x:130}},{t:this.shape_17},{t:this.shape_16,p:{x:160.3}},{t:this.shape_15,p:{x:173.8,y:72.7}},{t:this.shape_14,p:{x:198.9}},{t:this.shape_13,p:{x:218.7,y:72.7}},{t:this.shape_12,p:{x:228.9,y:72.7}},{t:this.shape_11,p:{x:239.1,y:72.7}},{t:this.shape_10,p:{x:253.6}},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6,p:{x:312.2,y:72.7}},{t:this.shape_5,p:{x:326.3,y:72.7}},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2,p:{x:378.8}},{t:this.shape_1,p:{x:385.2}},{t:this.shape,p:{x:391.5}}]},1).to({state:[{t:this.shape_98},{t:this.shape_48,p:{y:18.8}},{t:this.shape_97},{t:this.shape_45,p:{x:32.5,y:18.8}},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_42,p:{x:91.8,y:18.8}},{t:this.shape_93},{t:this.shape_92},{t:this.shape_33,p:{x:139,y:18.8}},{t:this.shape_26,p:{x:153,y:18.8}},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_20,p:{x:290,y:18.8}},{t:this.shape_86},{t:this.shape_85},{t:this.shape_29,p:{x:39.9}},{t:this.shape_84},{t:this.shape_12,p:{x:60.6,y:45.7}},{t:this.shape_11,p:{x:70.8,y:45.7}},{t:this.shape_6,p:{x:80.9,y:45.7}},{t:this.shape_27,p:{x:92.5}},{t:this.shape_83},{t:this.shape_82},{t:this.shape_34,p:{x:143}},{t:this.shape_59,p:{x:162,y:45.6}},{t:this.shape_81},{t:this.shape_15,p:{x:198.8,y:45.7}},{t:this.shape_5,p:{x:215.8,y:45.7}},{t:this.shape_52,p:{x:235.3,y:45.6}},{t:this.shape_80},{t:this.shape_79},{t:this.shape_13,p:{x:282.2,y:45.7}},{t:this.shape_78},{t:this.shape_51,p:{x:11.7,y:72.8}},{t:this.shape_77},{t:this.shape_23,p:{x:51.1}},{t:this.shape_16,p:{x:67.9}},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_10,p:{x:205.6}},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_14,p:{x:281.9}},{t:this.shape_18,p:{x:302.6}},{t:this.shape_67},{t:this.shape_24,p:{x:337.9}},{t:this.shape_2,p:{x:356.7}},{t:this.shape_1,p:{x:363.1}},{t:this.shape,p:{x:369.4}}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.flame = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_15 = function() {
		this.gotoAndPlay(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(15).call(this.frame_15).wait(1));

	// light
	this.instance = new lib.light("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.91,y:-3.5},2).to({scaleX:1.31,scaleY:0.83,y:2.3},2).to({scaleX:1.39,scaleY:1.39,y:0},1).to({regX:0.1,regY:0.1,scaleX:0.96,scaleY:0.8,x:1.9,y:2.4},2).to({regX:0,regY:0,scaleX:0.51,scaleY:0.83,x:-0.7,y:-4},4).to({scaleX:0.93,scaleY:0.8,x:-2.5,y:1.1},2).to({scaleX:1,scaleY:1,x:0,y:0},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30.7,-30.7,61.5,61.5);


// stage content:
(lib.front = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// headline
	this.instance = new lib.headline();
	this.instance.parent = this;
	this.instance.setTransform(-3,140.3,1,1.024,0,0,0,0,0.2);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(25).to({_off:false},0).wait(1).to({regX:197.9,regY:50.5,x:197.9,y:191.8,alpha:0.02},0).wait(1).to({x:200.9,alpha:0.041},0).wait(1).to({x:203.8,alpha:0.061},0).wait(1).to({x:206.7,alpha:0.082},0).wait(1).to({x:209.4,alpha:0.102},0).wait(1).to({x:212.2,alpha:0.122},0).wait(1).to({x:214.8,alpha:0.143},0).wait(1).to({x:217.5,alpha:0.163},0).wait(1).to({x:220,alpha:0.184},0).wait(1).to({x:222.5,alpha:0.204},0).wait(1).to({x:225,alpha:0.224},0).wait(1).to({x:227.4,alpha:0.245},0).wait(1).to({x:229.7,alpha:0.265},0).wait(1).to({x:232,alpha:0.286},0).wait(1).to({x:234.2,alpha:0.306},0).wait(1).to({x:236.4,alpha:0.327},0).wait(1).to({x:238.5,alpha:0.347},0).wait(1).to({x:240.6,alpha:0.367},0).wait(1).to({x:242.6,alpha:0.388},0).wait(1).to({x:244.6,alpha:0.408},0).wait(1).to({x:244.9,alpha:0.429},0).wait(1).to({alpha:0.449},0).wait(1).to({alpha:0.469},0).wait(1).to({alpha:0.49},0).wait(1).to({alpha:0.51},0).wait(1).to({alpha:0.531},0).wait(1).to({alpha:0.551},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.592},0).wait(1).to({alpha:0.612},0).wait(1).to({alpha:0.633},0).wait(1).to({alpha:0.653},0).wait(1).to({alpha:0.673},0).wait(1).to({alpha:0.694},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.735},0).wait(1).to({alpha:0.755},0).wait(1).to({alpha:0.776},0).wait(1).to({alpha:0.796},0).wait(1).to({alpha:0.816},0).wait(1).to({alpha:0.837},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.878},0).wait(1).to({alpha:0.898},0).wait(1).to({alpha:0.918},0).wait(1).to({alpha:0.939},0).wait(1).to({alpha:0.959},0).wait(1).to({alpha:0.98},0).wait(1).to({alpha:1},0).wait(51));

	// flame
	this.instance_1 = new lib.flame();
	this.instance_1.parent = this;
	this.instance_1.setTransform(679.7,295.6,1,1.024);
	this.instance_1.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(125));

	// bg
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAkAfIAAglQgBgIgEgFQgEgFgHAAQgNAAgEAPIAAACIAAAmIgFAAIAAglQAAgIgFgFQgEgFgHAAQgNAAgEAOIAAApIgHAAIAAg8IAHAAIAAAIQAHgJAMAAQAMAAAEALQAIgLANAAQAJAAAGAGQAGAHABAKIAAAmg");
	this.shape.setTransform(165.6,458.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgWAWQgIgJAAgNQAAgMAIgJQAKgKAMAAQAOAAAIAKQAJAJAAAMQAAANgJAJQgIAKgOAAQgMAAgKgKgAgQgRQgHAHAAAKQAAALAHAHQAGAHAKAAQAKAAAIgHQAGgHAAgLQAAgKgGgHQgIgHgKAAQgKAAgGAHg");
	this.shape_1.setTransform(156.4,458.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgRAWQgJgJAAgNQAAgMAJgJQAJgKAMAAQANAAAKAJIgFAFQgHgHgLAAQgJAAgHAHQgHAIAAAJQAAAKAHAIQAHAHAJAAQALAAAHgHIAFAFQgKAJgNAAQgMAAgJgKg");
	this.shape_2.setTransform(149.4,458.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgEAFQgCgCAAgDQAAgCACgCQACgCACAAQADAAACACQACACAAACQAAADgCACQgCACgDAAQgCAAgCgCg");
	this.shape_3.setTransform(144.5,461.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgTAWQgJgJAAgNQAAgMAJgJQAIgKAMAAQAMAAAHAIQAJAJAAAOIgyAAQAAALAHAHQAHAHAJAAQALAAAHgHIAFAFQgKAJgNAAQgMAAgJgKgAAVgFQgBgJgGgFQgFgFgJAAQgHAAgGAFQgGAFgBAJIApAAIAAAAg");
	this.shape_4.setTransform(139.4,458.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AASAfIAAglQAAgIgFgFQgEgFgIAAQgOAAgEAPIAAAoIgHAAIAAg8IAHAAIAAAJQAGgKAOAAQAJAAAGAGQAHAHAAAKIAAAmg");
	this.shape_5.setTransform(132.3,458.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgCAsIAAg9IAGAAIAAA9gAgCggQgDgCAAgDQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQAAgBABAAQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAAAQABABAAAAQABAAAAABQAAAAABABQAAAAABABQAAAAAAABQAAAAAAABQAAADgCACg");
	this.shape_6.setTransform(127.3,457.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgdArIAAhUIAHAAIAAAJQAJgKANAAQAMAAAJAKQAJAJAAAOQAAAMgJAIQgJAKgMAAQgNAAgJgKIAAAggAgNggQgGAEgDAFIAAAZQAHANAPAAQAJAAAHgIQAHgHAAgJQAAgLgHgHQgHgIgJAAQgHAAgGADg");
	this.shape_7.setTransform(122.1,460);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgCAsIAAhXIAFAAIAABXg");
	this.shape_8.setTransform(116.5,457.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgUAWQgJgJAAgNQAAgMAJgJQAJgKAMAAQANAAAJAKIAAgJIAHAAIAAA9IgHAAIAAgJQgJAKgNAAQgMAAgJgKgAgPgRQgHAIAAAJQAAALAHAHQAHAHAJAAQAPAAAHgMIAAgZQgHgMgPAAQgJAAgHAHg");
	this.shape_9.setTransform(110.9,458.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAKAoQgPAAAAgQIAAgmIgLAAIgBgHIAMAAIAAgOIAFgEIAAASIASAAIgBAHIgRAAIAAAlQAAAGADACQACACAFAAIAHAAIAAAHg");
	this.shape_10.setTransform(105.1,458);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgLAdQgGgEgCgFIAFgEQAFAJAJAAQAGAAAEgCQAEgDAAgFQAAgEgDgDQgDgDgGgCIgFgCQgOgEAAgNQAAgGAFgEQAGgFAGAAQANAAAEAMIgFACQgDgHgJAAQgDAAgDACQgEADAAAEQAAAIAKADIAFACQAPAFAAAMQAAAHgFAFQgGAFgJAAQgGAAgFgDg");
	this.shape_11.setTransform(100.3,458.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgTAWQgJgJAAgNQAAgMAJgJQAIgKAMAAQAMAAAHAIQAJAJAAAOIgyAAQAAALAHAHQAHAHAJAAQALAAAHgHIAFAFQgKAJgNAAQgMAAgJgKgAAVgFQgBgJgGgFQgFgFgJAAQgHAAgGAFQgGAFgBAJIApAAIAAAAg");
	this.shape_12.setTransform(94.2,458.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgWAWQgIgJAAgNQAAgMAIgJQAJgKANAAQAOAAAIAKQAJAJAAAMQAAANgJAJQgIAKgOAAQgNAAgJgKgAgRgRQgGAHAAAKQAAALAGAHQAIAHAJAAQAKAAAIgHQAGgHAAgLQAAgKgGgHQgIgHgKAAQgJAAgIAHg");
	this.shape_13.setTransform(86.8,458.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgdgeIAIAAIAVAvIAXgvIAIAAIgfA9g");
	this.shape_14.setTransform(79.8,458.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgEAFQgCgCAAgDQAAgCACgCQACgCACAAQADAAACACQACACAAACQAAADgCACQgCACgDAAQgCAAgCgCg");
	this.shape_15.setTransform(75.1,461.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAQAfIgQgrIgPArIgEAAIgTg8IAGAAIAPAwIARgxIASAxIAPgwIAGAAIgTA8g");
	this.shape_16.setTransform(69.3,458.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAQAfIgQgrIgPArIgEAAIgTg8IAGAAIAQAwIAQgxIASAxIAPgwIAGAAIgTA8g");
	this.shape_17.setTransform(60.7,458.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAQAfIgQgrIgPArIgEAAIgTg8IAGAAIAPAwIARgxIASAxIAPgwIAGAAIgTA8g");
	this.shape_18.setTransform(52.2,458.8);

	this.instance_2 = new lib.front_1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,0,1,1.024);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(125));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(512,256,1024,512);
// library properties:
lib.properties = {
	id: 'F6F2BA9C72734BD3A7DA14FD25435EE8',
	width: 1024,
	height: 512,
	fps: 25,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/img/front_1.jpg", id:"front_1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['F6F2BA9C72734BD3A7DA14FD25435EE8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;