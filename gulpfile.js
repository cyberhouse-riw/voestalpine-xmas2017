var json5 = require('json5');
var gulp = require('gulp');
var gutil = require('gulp-util');
var minimist = require('minimist');
var gulpif = require('gulp-if');
var newer = require('gulp-newer');
var del = require('del');
var globby = require('globby');
var rollup = require('rollup-stream');
var source = require('vinyl-source-stream2');
var merge = require('merge-stream');
var scss = require('gulp-sass');
var postcss = require('gulp-postcss');
var nunjucks = require('gulp-nunjucks-render');
var fs = require('fs');
var path = require('path');
var sequence = require('run-sequence');

// arguments

var argv = minimist(process.argv.slice(2), {
  boolean: 'clean',
  string: 'mode'
});

// HELPER FUNCTIONS

// logging
var logError = function (err) {
  gutil.log(gutil.colors.red(err.message));
  return err.message;
};
try {
  var notify = require('gulp-notify');
  notify.logLevel(0);
  logError = notify.onError(logError);
}
catch(err) {
}

function logNothing(n) {
  gutil.log('Nothing to do in task ' + gutil.colors.cyan("'" + n + "'"));
}

// testing

function isString(s) {
  return (typeof s === 'string' || s instanceof String);
}

function isObject(o) {
  return (Object.prototype.toString.call(o) === '[object Object]');
}

function isFunction(f) {
  return (Object.prototype.toString.call(f) === '[object Function]');
}

function isTask(t) {
  return (isObject(t) && t.dest);
}

function ifSetElse(a, b) {
  return (a !== undefined ? a : b);
}

// resolving

function getMode() {
  if (modes.list.indexOf(argv.mode) < 0) {
    return modes.default;
  }
  else {
    return argv.mode;
  }
}

function resolveObject(obj, mode) {
  var keys = Object.keys(obj);
  var i = keys.length, resolve = false;
  while (i--) {
    resolve = (modes.list.indexOf(keys[i]) >= 0);
    if (!resolve) {
      break;
    }
  }
  if (resolve) {
    return obj[mode];
  }
  else {
    return obj;
  }
}

function resolve(obj, mode) {
  if (Array.isArray(obj)) {
    return obj.map(function (obj) {
      return resolve(obj, mode);
    });
  }
  else if (isObject(obj)) {
    obj = resolveObject(obj, mode);
    if (isObject(obj)) {
      return Object.keys(obj).reduce(function(newobj, key) {
        newobj[key] = resolve(obj[key], mode);
        return newobj;
      }, {});
    }
    else {
      return resolve(obj, mode);
    }
  }
  else if (configx !== undefined && isString(obj) && obj.substr(0, 2) === 'x:') {
    return configx[obj.substr(2)];
  }
  return obj;
}

function resolveFunctions(obj, params) {
  if(isFunction(obj)) {
    return obj.apply(null, params);
  }
  else if(Array.isArray(obj)) {
    return obj.map(function (obj) {
      return resolveFunctions(obj, params);
    });
  }
  else if (isObject(obj)) {
    return Object.keys(obj).reduce(function(newobj, key) {
      newobj[key] = resolveFunctions(obj[key], params);
      return newobj;
    }, {});
  }
  return obj;
}

function resolveConfig(config, mode) {
  var time = Date.now();
  gutil.log('Resolving config in mode ' + gutil.colors.cyan("'" + mode + "'") + '...');
  config = resolve(config, mode);
  config = resolveFunctions(config, [config, mode]);
  gutil.log('Finished resolving config after ' + gutil.colors.magenta((Date.now() - time) + ' ms'));
  return config;
}

// handle tasks
function runTask(name, func, cb) {
  var task = config[name];
  if (Array.isArray(task)) {
    var streams = [];
    task.forEach(function (task) {
      if (isTask(task)) {
        streams.push(func(task));
      }
    });
    if (streams.length > 0) {
      return merge.apply(null, streams);
    }
    else {
      logNothing(name);
      cb();
    }
  }
  else if (isTask(task)) {
    return func(task);
  }
  else {
    logNothing(name);
    cb();
  }
}

function taskErrorHandler(err) {
  logError(err);
  this.emit('end');
}

function finalizeTask(stream, dest) {
  if (Array.isArray(dest)) {
    dest.forEach(function (dest) {
      stream.pipe(gulp.dest(dest));
    });
  }
  else {
    stream.pipe(gulp.dest(dest));
  }
  return stream;
}

function getSrc(task, attribute, elseAttribute) {
  if (Array.isArray(task)) {
    var src = [];
    task.forEach(function (task) {
      if (isTask(task)) {
        var s = ifSetElse(task[attribute], task[elseAttribute]);
        if (Array.isArray(s)) {
          src = src.concat(s);
        }
        else {
          src.push(s);
        }
      }
    });
    if (src.length > 0) {
      return src;
    }
    else {
      return null;
    }
  }
  else if (isTask(task)) {
    return ifSetElse(task[attribute], task[elseAttribute]);
  }
  else {
    return null;
  }
}

function newerOnly(task, map) {
  var options = {};
  if(task.watch !== undefined) {
    options.extra = task.watch;
  }
  if(Array.isArray(task.dest)) {
    options.dest = task.dest[0];
  }
  else {
    options.dest = task.dest;
  }
  if(map) {
    if (map.charAt(0) === '.') {
      options.ext = map;
    }
    else {
      options.map = function(p) {
        return map;
      };
    }
  }
  return gulpif(
    !watching && (options.extra || task.src).indexOf(options.dest) !== 0,
    newer(options)
  );
}

// templates

function nunjucksManageEnv(env) {
  var task = this;

  if (isObject(task.options.nunjucks.filters)) {
    var filters = task.options.nunjucks.filters;
    Object.keys(filters).forEach(function(name) {
      env.addFilter(name, filters[name]);
    });
  }
}

// watching

var watching = false;

function watch(key, cb) {
  watching = true;
  var src = getSrc(config[key], 'watch', 'src');
  if (src !== null) {
    gulp.watch(src, [key]);
  }
  else {
    logNothing('watch-' + key);
  }
  cb();
}

// preparing

var modes;
try {
  modes = json5.parse(fs.readFileSync('./modes.json', 'UTF8'));
}
catch (err) {
  modes = {
    list: ['dev', 'prod'],
    default: 'dev',
    deploy: 'prod'
  };
}

var mode = getMode();
var config;
try {
  config = json5.parse(fs.readFileSync('./config.json', 'UTF8'));
}
catch (err) {
  logError(err);
  process.exit();
}
var configx
try {
  configx = require('./configx.js');
}
catch(err) {
}

config = resolveConfig(config, mode);

// TASKS

// basic tasks

gulp.task('clean', function (cb) {
  var src = [];
  for (key in config) {
    var s = getSrc(config[key], 'clean', 'dest');
    if (s !== null) {
      if (Array.isArray(s)) {
        src = src.concat(s);
      }
      else {
        src.push(s);
      }
    }
  }
  del.sync(src, { force: true });
  cb();
});

var rollupCache = {};
var through = require('through2');

gulp.task('js', function (cb) {
  return runTask('js', function (task) {
    if (isObject(task.options.rollup)) {
      return finalizeTask(gulp
        .src(task.src, {read: false})
        .pipe(newerOnly(task))
        .pipe(through.obj(function(file, enc, cb) {
          return finalizeTask(rollup(Object.assign({}, task.options.rollup, {
            input: file.path,
            cache: rollupCache[file.path]
          }))
            .on('error', taskErrorHandler)
            .on('bundle', function(bundle) {
              if(watching) {
                rollupCache[file.path] = bundle;
              }
            })
            .pipe(source(file))
            .on('error', taskErrorHandler), task.dest)
            .on('end', cb);
        })), task.dest);
    }
    else {
      return finalizeTask(gulp
        .src(task.src)
        .pipe(newerOnly(task)), task.dest);
    }
  }, cb);
});

gulp.task('css', function (cb) {
  return runTask('css', function (task) {
    return finalizeTask(gulp
      .src(task.src)
      .pipe(newerOnly(task, '.css'))
      .pipe(gulpif(
        isObject(task.options.scss),
        scss(task.options.scss)
      ))
      .on('error', taskErrorHandler)
      .pipe(gulpif(
        Array.isArray(task.options.postcss),
        postcss(task.options.postcss)
      ))
      .on('error', taskErrorHandler), task.dest);
  }, cb);
});

gulp.task('templates', function (cb) {
  return runTask('templates', function (task) {
    return finalizeTask(gulp
      .src(task.src)
      .pipe(newerOnly(task))
      .pipe(gulpif(
        isObject(task.options.nunjucks),
        nunjucks(Object.assign({}, task.options.nunjucks, {
          manageEnv: nunjucksManageEnv.bind(task)
        }))
      ))
      .on('error', taskErrorHandler), task.dest);
  }, cb);
});

gulp.task('copy', function (cb) {
  return runTask('copy', function (task) {
    return finalizeTask(gulp
      .src(task.src)
      .pipe(newerOnly(task)), task.dest);
  }, cb);
});

// watch tasks

gulp.task('watch-img', function (cb) {
  watch('img', cb);
});

gulp.task('watch-js', function (cb) {
  watch('js', cb);
});

gulp.task('watch-css', function (cb) {
  watch('css', cb);
});

gulp.task('watch-templates', function (cb) {
  watch('templates', cb);
});

gulp.task('watch-copy', function (cb) {
  watch('copy', cb);
});

// dev tasks

var favicons;
try {
  favicons = require('gulp-favicons');
}
catch(err) {
  favicons = err;
}
gulp.task('favicons', function (cb) {
  if(favicons instanceof Error) {
    logError(favicons);
    cb();
  }
  else {
    return runTask('favicons', function (task) {
      return finalizeTask(gulp
        .src(task.src)
        .pipe(newerOnly(task, '.ico'))
        .pipe(gulpif(
          isObject(task.options.favicons),
          favicons(task.options.favicons, function (err) {
            if (err) {
              logError(err);
            }
          })
        )), task.dest);
    }, cb);
  }
});

var imagemin;
try {
  imagemin = require('gulp-imagemin');
}
catch(err) {
  imagemin = err;
}
gulp.task('img', function (cb) {
  if(imagemin instanceof Error) {
    logError(imagemin);
    cb();
  }
  else {
    return runTask('img', function (task) {
      return finalizeTask(gulp
        .src(task.src)
        .pipe(newerOnly(task))
        .pipe(gulpif(
          isObject(task.options.imagemin),
          imagemin(task.options.imagemin)
        ))
        .on('error', taskErrorHandler), task.dest);
    }, cb);
  }
});

var browsersync;
try {
  browsersync = require('browser-sync');
}
catch(err) {
  browsersync = err;
}
gulp.task('serve', ['watch'], function (cb) {
  if(browsersync instanceof Error) {
    logError(browsersync);
  }
  else {
    var task = config['serve'];

    if (isObject(task)) {
      browsersync = browsersync.create();
      browsersync.init(task.browsersync);
    }
  }
  cb();
});

var rsync;
try {
  rsync = require('gulp-rsync');
}
catch(err) {
  rsync = err;
}
gulp.task('deploy', ['default'], function (cb) {
  if(rsync instanceof Error) {
    logError(rsync);
    cb();
  }
  else {
    var task = config['deploy'];

    return gulp.src(task.src)
      .pipe(rsync(task.rsync))
      .on('error', taskErrorHandler);
  }
});

// combined tasks

gulp.task('default', function (cb) {
  var tasks = (argv.clean ? ['clean']: []);
  tasks.push(['favicons', 'img', 'js', 'css'], 'templates', 'copy', cb);
  sequence.apply(null, tasks);
});

gulp.task('watch', function (cb) {
  sequence('default', ['watch-img', 'watch-js', 'watch-css', 'watch-templates', 'watch-copy'], cb);
});